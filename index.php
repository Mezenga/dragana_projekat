<?php
/**
 * Created by Dragana Cajic.
 * User: Dragana
 */
    require_once 'config/loader.php';
	require_once 'config/loader_models.php';
	$usersModel=new usersModel;
?>
<!DOCTYPE html>
<html style="height: 100%;">
<?php
    require_once FULL_FILE_PATH.'Views/Default/head.php';
?>
    <body style="background-color:lightgoldenrodyellow; height: 100%;">
        <?php
   // echo password_hash('123',PASSWORD_DEFAULT);
            if(isset($_SESSION["is_logged"]))
            {
                require_once FULL_FILE_PATH."Views/Default/header_section.php";
                if(isset($_GET['view']))
                {
                    if($_GET['view']=='userslist' && $_SESSION['role']==1 && !isset($_GET['reg']))
                    {
                        $view='withoutview.php';
                    }
                    else
                    {
                        $view=$_GET['view'].'.php';
                    }

                    $viewPath = FULL_FILE_PATH."Views/".$view;
                    if(file_exists($viewPath))
                    {

                        include $viewPath;
                    }

                }
                else
                {
                       $path = FULL_FILE_PATH."Views/withoutview.php";
                       include $path;
                }

            }
            else
            {
                    if(!isset($_GET['login']))
                    {

                        if(isset($_GET['view']) && $_GET['view']=='offerlist')
                        {
                            include FULL_FILE_PATH."Views/offerlist.php";
                        }
                        if(isset($_GET['view']) && $_GET['view']=='detailsoffer' && isset($_GET['id']))
                        {
                            include FULL_FILE_PATH."Views/detailsoffer.php";

                        }
                        if(isset($_GET['view']) && $_GET['view']=='details' && isset($_GET['id']))
                        {

                            include FULL_FILE_PATH."Views/details.php";
                        }
                        else
                        {

                            include FULL_FILE_PATH."Views/firstpage.php";
                        }

                    }
                    else
                    {
                            include FULL_FILE_PATH."Views/login.php";
                    }
            }
        ?>
    </body>
</html>
