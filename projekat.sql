-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2018 at 01:03 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projekat`
--

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE `korisnik` (
  `idKorisnika` int(11) NOT NULL,
  `idVrste` int(11) DEFAULT NULL,
  `idUloge` int(11) DEFAULT NULL,
  `naziv` text,
  `email` text,
  `lozinka` text,
  `kontakt_adresa` text,
  `web_sajt` text,
  `odobren_pristup` tinyint(1) DEFAULT NULL,
  `opis_djelatnosti_korisnika` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`idKorisnika`, `idVrste`, `idUloge`, `naziv`, `email`, `lozinka`, `kontakt_adresa`, `web_sajt`, `odobren_pristup`, `opis_djelatnosti_korisnika`) VALUES
(109, 6, 1, 'Lanaco', 'lll@yahoo.com', '$2y$10$PqanphKU/7RnyAGEU7tkD.4O1ym7hY8ZEL9XVZO7PI9/cgdXnGUKi', 'Banja Luka', 'www.lanaco.com', 1, 'Opis Lanaco'),
(110, 3, 1, 'Comtrade', 'ccc@yahoo.com', '$2y$10$aULIFsnWl9DIXQ4JXKJFAexKGsY.rf/A537ldACRvVw6idjfzgmvS', 'Sarajevo', 'www.comtrade.com', 1, 'Opis Comtrade'),
(111, NULL, 2, 'Admin', 'draganacajic95@gmail.com', '$2y$10$XbkOcOfQ1Ye.PzcFdru/6OiqV1n6YDx9P9BMBNZT9xmIGEMd9TEXi', 'Lukavica', 'www.etf.ues.rs.ba', 1, ''),
(113, 5, 1, 'Authority', 'aaa@yahoo.com', '$2y$10$Uo9ZYW.NruipQKO0EiKpIeMCoYAGXtnKB0ZD/cTNSztmBisDZxGHq', 'Banja Luka', 'www.authority.com', 1, 'Opis Authority'),
(115, 1, 1, 'test', 'atlant@yahoo.com', '$2y$10$WiUeYvR3J5F.L/gYo9SMSui1a8htznhSuor6hmG4vxcyAumAQSYBi', 'Sarajevo', 'www.atlant.com', 1, 'Opis Atlant'),
(117, 6, 1, 'Itis', 'iii@yahoo.com', '$2y$10$bFFh5t8hDiTyoiyCJxHtMOiW4TZqh5NVChXX1lk9fLKbnisNRzG4e', 'Lukavica', 'www.itis.ba', 1, 'Opis itis'),
(118, 7, 1, 'DosAmigos', 'ddd@yahoo.com', '$2y$10$Jzl4yggZwWjFXo1eLeZ/i.O0BXkYxqE5MfNz2JZPxxFOI4JthfBza', 'Lukavica', 'www.dosamigos.com', 1, 'Opis dos amigos'),
(119, 5, 1, 'KV tim', 'kv@yahoo.com', '$2y$10$caKzuJXbqTgzm3A30x6ZnOhgn476iAq.yJ9Uwcm8ORet3WxVlAF1q', 'Lukavica', 'www.kv.com', 1, 'Opis KV'),
(120, 6, 1, 'Pragmatio Solution', 'ppp@yahoo.com', '$2y$10$lgiTWMsn2/W7rwnTegrXhuC0U.TxZWaLkvAoUqwifChiqfIG3uGiC', 'Sarajevo', 'www.pragmatio.com', 1, 'Opis Pragmatio'),
(121, 2, 1, 'NSoft', 'nnn@yahoo.com', '$2y$10$mmchLMBBzVEdi2oO2/XaaOUztZWaYTSFgWrVIpSyL3TG8M6pBqKp.', 'Mostar', 'www.nsoft.com', 1, 'Opis NSoft'),
(122, 7, 1, 'ZIRA', 'zzz@yahoo.com', '$2y$10$LVa5hF7WAbOEoeFNTgtte.lgPGSRSmgD3.p0CRDsKfCf/PkhV.eoe', 'sarajevo', 'www.zira.ba', 0, 'Opis Zira'),
(123, 1, 1, 'Planet Soft', 'planet@yahoo.com', '$2y$10$4DiuHfeur79FFHq23QOreuBe9u8t9RIhUm.2ETGgp5otBZMvJYr0.', 'Banja Luka', 'www.planet.com', 1, 'Opis Planet Sofft'),
(131, 2, 1, 'Klika', 'klika@yahoo.com', '$2y$10$z7/mrlbWNZ0aI5tccNKb0.n4OlmvxYlwgFgDLpFMvhE5yuV77fxLi', 'Sarajevo', 'www.klika.ba', 1, 'Opis Klika'),
(132, 1, 1, 'Walter', 'walter@yahoo.com', '$2y$10$YyHfhmQTRncxj1piCWc8qe5csJ5BLQC8KIV2jRYVwLdZ7Pzv5v1ya', 'Sarajevo', 'www.walter.ba', 1, 'Opis Walter'),
(133, 6, 1, 'RTRK', 'rtrtk@yahoo.com', '$2y$10$qfB6ja0lmbm2j//mEiu46.jn5GzNJqzv71eYcT9A1BqycDLr53RqW', 'Banja Luka', 'www.rtrk.bcom', 1, 'Opis RTRK'),
(144, 6, 1, 'K-inel', 'kkkkinle@yahoo.com', '$2y$10$cM6XAIiEagUotGuJtou6zOLho0nIRlTZCt9Hf8CpVANKnjBr0L1Qi', 'Lukavica', 'www.kinel.com', 1, 'Opis djelatnosti K-inel'),
(146, 1, 1, 'Simphony', 'draganacajic@yahoo.com', '$2y$10$IuvszcDNhLpnlj6xdPFY5.g4vHIjJ0/3Pth6ZpYwA35.piUO58AWe', 'Sarajevo', 'www.simphony.com', 1, 'Opis Simphony');

-- --------------------------------------------------------

--
-- Table structure for table `ponuda_poslodavca`
--

CREATE TABLE `ponuda_poslodavca` (
  `idPonude` int(11) NOT NULL,
  `idKorisnika` int(11) NOT NULL,
  `idTipa` int(11) NOT NULL,
  `ciklus_studija` text,
  `rok_za_prijavu` date DEFAULT NULL,
  `maksimalan_broj_kandidata` int(11) DEFAULT NULL,
  `lokacija` text,
  `trajanje` text,
  `pocetak` date NOT NULL,
  `opis_ponude` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ponuda_poslodavca`
--

INSERT INTO `ponuda_poslodavca` (`idPonude`, `idKorisnika`, `idTipa`, `ciklus_studija`, `rok_za_prijavu`, `maksimalan_broj_kandidata`, `lokacija`, `trajanje`, `pocetak`, `opis_ponude`) VALUES
(81, 109, 11, 'treca i cetvrta godina', '2018-09-08', 10, 'Banja Luka', 'jedna skolska godina', '2018-11-09', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean maximus justo est, ut sodales leo cursus et. Fusce ac viverra turpis, vitae accumsan mi. Etiam interdum ipsum ac nibh tincidunt lacinia. Curabitur rutrum leo in metus suscipit, quis pellentesque dui dignissim. Ut lacus elit, elementum at diam aliquet, suscipit aliquam ipsum. Maecenas efficitur viverra tellus vitae efficitur. Sed ac ultricies augue.  Vestibulum vitae tortor tincidunt, lacinia nulla vitae, faucibus erat. In fringilla quam nec nisl ultricies suscipit. Phasellus convallis lorem in augue iaculis molestie. Fusce iaculis enim enim, quis lacinia urna maximus sit amet. Etiam pharetra tempus feugiat. Quisque fringilla, nisi vel aliquet viverra, tellus felis finibus tortor, at sagittis nisl sapien id dolor. Quisque volutpat quis lectus vitae aliquam. Nam quis ultricies magna. Pellentesque interdum facilisis ante, faucibus rutrum felis ultricies nec. Integer tortor ante, vulputate consequat dignissim nec, maximus eget purus. Aenean viverra mi et metus vulputate suscipit id ac turpis. Suspendisse elementum nunc nec nisl malesuada ultrices. Pellentesque dolor sem, efficitur feugiat aliquam et, tincidunt at neque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.  Donec eu vulputate sem. Maecenas efficitur, justo non tempor tempor, enim risus dapibus libero, vitae finibus tortor felis in arcu. Duis sit amet volutpat libero, a condimentum est. In luctus, mauris et suscipit facilisis, leo ipsum finibus sem, maximus varius diam purus eget metus. Nulla semper tempor porttitor. Suspendisse volutpat, magna eget euismod blandit, velit massa gravida arcu, et egestas lectus eros ut justo. Phasellus rhoncus nulla accumsan ornare pellentesque. Vivamus et scelerisque lacus, a laoreet lorem. Nam mattis mi eros, nec faucibus orci facilisis non. Pellentesque et odio a velit posuere eleifend. Interdum et malesuada fames ac ante ipsum primis in faucibus.'),
(82, 109, 8, 'Cetvrta godina', '2018-12-15', 10, 'Lukavica', 'dvije sedmice', '2018-12-21', 'Opis radionice Lanaco'),
(83, 109, 9, 'Apsolvenit', '2018-12-01', 8, 'Istocno Sarajevo', 'neograniceno', '2018-12-01', 'Opis zavrsni rad Lanaco'),
(84, 113, 3, 'Cetvrta godina', '2018-11-10', 10, 'Sarajevo', 'dva mjeseca', '2018-12-28', 'Opis diplomski '),
(85, 113, 7, 'Treca i cetvrta godina', '2018-09-08', 9, 'Sarajevo', 'dvije sedmice', '2018-10-11', 'Opis ljetna skola Authority'),
(86, 113, 11, 'Treca i cetvrta godina', '2018-12-01', 10, 'Istocno Sarajevo', '6 mjeseci', '2018-11-03', 'Opis stipendija Authority'),
(87, 113, 4, 'Cetvrta godina', '2018-12-08', 15, 'Sarajevo', 'dva mjeseca', '2019-01-12', 'Ponuda prakse Authority'),
(88, 110, 9, 'Cetvrta godina', '2018-09-15', 8, 'Lukavica', '6 mjeseci', '2018-09-29', 'Opis zavrsni rad Comtrade'),
(89, 110, 8, 'Sve ', '2019-03-01', 8, 'Elektrotehnicki fakultet', 'dva mjeseca', '2018-09-29', 'Opis radionice Comtrade'),
(92, 132, 3, 'Prvi ciklus studija', '2019-01-18', 10, 'Sarajevo', '3 mjeseca', '2018-11-30', 'Opis diplomski rad od Klike'),
(93, 132, 6, 'Treca godina', '2018-09-14', 4, 'Istocno Sarajevo', '3 mjeseca', '2018-10-19', 'Opis projekat od Waltera'),
(94, 132, 7, 'Cetvrta godina', '2019-01-25', 15, 'Istocno Sarajevo', '15 dana', '2019-01-25', 'Opis ljetna skola od Waltera'),
(95, 132, 8, 'Treca i cetvrta godina', '2018-11-24', 10, 'Lukavica', 'dvije sedmice', '2019-01-25', 'Ponuda za radionicu od Waltera'),
(96, 132, 4, 'Cetvrta godina', '2019-01-18', 8, 'Sarajevo', 'dva mjeseca', '2018-11-30', 'Opis prakse od Waltera'),
(97, 132, 9, 'Apsolventi', '2018-11-24', 10, 'Sarajevo', '10 dana', '2018-12-07', 'Opis zavrsnog rada od Waltera'),
(98, 132, 3, 'Apsolventi', '2018-12-08', 5, 'Lukavica', '-', '2018-11-22', 'OPis diplomskog rada od Waltera'),
(99, 132, 11, 'Cetvrta godina', '2018-11-03', 10, 'Sarajevo', '6 mjeseci', '2019-01-01', 'Opis stipendije od Waltera'),
(100, 132, 1, 'treca i cetvrta godina', '2018-09-22', 10, 'Lukavica', '3 mjeseca', '2018-10-05', 'Opis seminarskog rada od Waltera'),
(101, 132, 3, 'cetvrta godina', '2018-11-10', 10, 'Lukavica', '-', '2018-11-24', 'Opis diplomskog rada od Waltera'),
(102, 132, 4, 'cetvrta godina', '2018-12-08', 6, 'Sarajevo', '3 mjeseca', '2018-12-15', 'OPis prakse od Waltera'),
(103, 132, 11, 'cetvrta godina', '2018-12-01', 16, 'Sarajevo', 'jedna skolska godina', '2018-11-23', 'Opis stipendije od Waltera');

-- --------------------------------------------------------

--
-- Table structure for table `sifrarnik_tipova_aktivnosti`
--

CREATE TABLE `sifrarnik_tipova_aktivnosti` (
  `idTipa` int(11) NOT NULL,
  `naziv_tipa_aktivnosti` text,
  `opis_aktivnosti` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sifrarnik_tipova_aktivnosti`
--

INSERT INTO `sifrarnik_tipova_aktivnosti` (`idTipa`, `naziv_tipa_aktivnosti`, `opis_aktivnosti`) VALUES
(1, 'Seminarski rad', 'opis Seminarski rad'),
(3, 'Diplomski rad', 'opis Diplomski rad'),
(4, 'Praksa', 'opis Praksa'),
(6, 'Projekat', 'opis Projekat'),
(7, 'Ljetna skola', 'opis Ljetna skola'),
(8, 'Radionica', 'opis Radionica'),
(9, 'Zavrsni rad', 'opis Zavrsni rad'),
(11, 'Stipendija', 'opis Stipendija');

-- --------------------------------------------------------

--
-- Table structure for table `sifrarnik_vrste_djelatnosti`
--

CREATE TABLE `sifrarnik_vrste_djelatnosti` (
  `idVrste` int(11) NOT NULL,
  `naziv_vrste_djelatnosti` text,
  `opis_djelatnosti` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sifrarnik_vrste_djelatnosti`
--

INSERT INTO `sifrarnik_vrste_djelatnosti` (`idVrste`, `naziv_vrste_djelatnosti`, `opis_djelatnosti`) VALUES
(1, 'Informatika', 'Opis informatika'),
(2, 'Gradjevinarstvo', 'Opis gradjevinarstvo'),
(3, 'Lokalna uprava', 'Opis lokalna uprava'),
(5, 'Automatika', 'Opis automatika'),
(6, 'Elektronika', 'Opis elektronika'),
(7, 'Elektroenergetika', 'Opis elektroenergetika');

-- --------------------------------------------------------

--
-- Table structure for table `uloge`
--

CREATE TABLE `uloge` (
  `idUloge` int(11) NOT NULL,
  `naziv_uloge` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uloge`
--

INSERT INTO `uloge` (`idUloge`, `naziv_uloge`) VALUES
(1, 'Poslodavac'),
(2, 'Administrator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD PRIMARY KEY (`idKorisnika`),
  ADD KEY `FK_IZBOR_VRSTE_DJELATNOSTI` (`idVrste`),
  ADD KEY `FK_ULOGA_KORISNIKA` (`idUloge`);

--
-- Indexes for table `ponuda_poslodavca`
--
ALTER TABLE `ponuda_poslodavca`
  ADD PRIMARY KEY (`idPonude`),
  ADD KEY `FK_IZBOR_TIPA_AKTIVNOSTI` (`idTipa`),
  ADD KEY `FK_PONUDA_POSLODAVCA` (`idKorisnika`);

--
-- Indexes for table `sifrarnik_tipova_aktivnosti`
--
ALTER TABLE `sifrarnik_tipova_aktivnosti`
  ADD PRIMARY KEY (`idTipa`);

--
-- Indexes for table `sifrarnik_vrste_djelatnosti`
--
ALTER TABLE `sifrarnik_vrste_djelatnosti`
  ADD PRIMARY KEY (`idVrste`);

--
-- Indexes for table `uloge`
--
ALTER TABLE `uloge`
  ADD PRIMARY KEY (`idUloge`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `korisnik`
--
ALTER TABLE `korisnik`
  MODIFY `idKorisnika` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `ponuda_poslodavca`
--
ALTER TABLE `ponuda_poslodavca`
  MODIFY `idPonude` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `sifrarnik_tipova_aktivnosti`
--
ALTER TABLE `sifrarnik_tipova_aktivnosti`
  MODIFY `idTipa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sifrarnik_vrste_djelatnosti`
--
ALTER TABLE `sifrarnik_vrste_djelatnosti`
  MODIFY `idVrste` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `uloge`
--
ALTER TABLE `uloge`
  MODIFY `idUloge` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD CONSTRAINT `FK_IZBOR_VRSTE_DJELATNOSTI` FOREIGN KEY (`idVrste`) REFERENCES `sifrarnik_vrste_djelatnosti` (`idVrste`),
  ADD CONSTRAINT `FK_ULOGA_KORISNIKA` FOREIGN KEY (`idUloge`) REFERENCES `uloge` (`idUloge`);

--
-- Constraints for table `ponuda_poslodavca`
--
ALTER TABLE `ponuda_poslodavca`
  ADD CONSTRAINT `FK_IZBOR_TIPA_AKTIVNOSTI` FOREIGN KEY (`idTipa`) REFERENCES `sifrarnik_tipova_aktivnosti` (`idTipa`),
  ADD CONSTRAINT `FK_PONUDA_POSLODAVCA` FOREIGN KEY (`idKorisnika`) REFERENCES `korisnik` (`idKorisnika`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
