<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/22/18
 * Time: 8:39 AM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'/config/database.php';
class offerModel
{
    /**
     * Function which return list of offers from database
     * @param bool $id If we want one offer we can send id of that offer.
     * @param bool $limit_clause Limit how much offers we want.
     * @return array|mixed List of offers.
     */
    public function offerList($id=FALSE,$limit_clause = FALSE)
    {
        try
        {
            if($id!=FALSE)
            {
                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                 ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.idPonude=".$id;
                $query = database::connection()->prepare($sql);
                $query->execute();
                $result=$query->fetch();
            }
            else
            {
               $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                 ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa ORDER BY ponuda_poslodavca.rok_za_prijavu";
                $query = database::connection()->prepare($sql);
                $query->execute();
                $result=$query->fetchAll();
            }
            if($limit_clause!=FALSE)
            {
                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                 ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                $query = database::connection()->prepare($sql);
                $query->execute();
                $result=$query->fetchAll();
            }
            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

    /**
     * Function for change information about offer.
     * @param string $data New information about offer.
     */
    public function updateOffer($data)
    {
        try
        {
            $sql="UPDATE ponuda_poslodavca SET idTipa=:offer_type,ciklus_studija=:study,rok_za_prijavu=:deadline,maksimalan_broj_kandidata=:max_number,lokacija=:location,trajanje=:duration,pocetak=:start,opis_ponude=:offer_description WHERE idPonude=:id";
            $query = database::connection()->prepare($sql);
            $query->execute(array('offer_type'=>$data['offer_type'],'study'=>$data['study'],'deadline'=>$data['deadline_check_in'],'max_number'=>$data['max_number'],'location'=>$data['location'],'duration'=>$data['duration'],'start'=>$data['start'],'offer_description'=>$data['description_offer'],'id'=>$data['id']));
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function for insert new offer in database.
     * @param string $data Information about new offer.
     */
    public function addOffer($data)
    {
        try
        {
            $sql="INSERT INTO ponuda_poslodavca(idKorisnika,idTipa,ciklus_studija,rok_za_prijavu,maksimalan_broj_kandidata,lokacija,trajanje,pocetak,opis_ponude) VALUES ('".$_SESSION['id']."','".$data['add_offer_type']."','".$data['add_study']."','".$data['add_deadline_check_in']."','".$data['add_max_number']."','".$data['add_location']."','".$data['add_duration']."','".$data['add_start']."','".$data['add_description_offer']."')";
            $query = database::connection()->prepare($sql);
            $query->execute();

        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function which remove offer from database.
     * @param string $id Id of offer which we want to delete.
     */
    public function deleteOffer($id)
    {
        try
        {
            $sql="DELETE FROM ponuda_poslodavca WHERE idPonude=:id";
            $query = database::connection()->prepare($sql);
            $query->execute(array(':id'=>$id));

        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function for searching special offer.
     * @param string $data Word or letter for searching.
     * @param bool $limit_clause Limit how much offers we want.
     * @return array Found offers.
     */
    public function searchOffer($data,$limit_clause=FALSE)
    {
        try
        {
            if($limit_clause!=FALSE)
            {
                $sql_type="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                 ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa
                 AND (naziv_tipa_aktivnosti LIKE '%".$data."%' OR lokacija LIKE '%".$data."%' OR naziv LIKE '%".$data."%') ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result=$query->fetchAll();
            }
            else
            {
                $sql_type="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                 ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa
                 AND (naziv_tipa_aktivnosti LIKE '%".$data."%' OR lokacija LIKE '%".$data."%' OR naziv LIKE '%".$data."%') ORDER BY ponuda_poslodavca.rok_za_prijavu";
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result=$query->fetchAll();
            }

            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

    /**
     * Function which return offers of special user.
     * @param string $id Id of user whose offers we want.
     * @param bool $limit_clause Number of how much offers we want.
     * @return array Offers of some user.
     */
    public function offersOfUser($id,$limit_clause=FALSE)
    {
        try
        {
            if($limit_clause!=FALSE)
            {
                 $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                 ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.idKorisnika='".$id."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
            }
            else
            {
                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.idKorisnika='".$id."' ORDER BY ponuda_poslodavca.rok_za_prijavu";
            }
            $query = database::connection()->prepare($sql);
            $query->execute();
            $result=$query->fetchAll();
            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

    /**
     * Function for filter offers from database.
     * @param bool $user Name of user whose offers we want to see.
     * @param bool $location Location of offers which we want to see.
     * @param bool $active Active offers are offers whose the deadline for the check-in has not expired.
     * @param bool $limit_clause Number of how much offers we want to see.
     * @return array|string Offers which is filtered.
     */
    public function filterOffer($user=FALSE,$location=FALSE,$active=FALSE,$limit_clause=FALSE,$search=FALSE)
    {
        try
        {
            $result="";
            $date_now=date("Y-m-d");
            if($search==FALSE)
            {
                if($limit_clause==FALSE)
                {
                    if($active!=FALSE)
                    {
                        if($active=='active')
                        {
                            if($user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND korisnik.naziv='".$user."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE && $user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location==FALSE && $user==FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                        }
                        if($active=='non_active')
                        {
                            if($user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND korisnik.naziv='".$user."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE && $user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location==FALSE && $user==FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                        }

                    }
                    else
                    {
                        if($user!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND korisnik.naziv='".$user."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location!=FALSE && $user!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location==FALSE && $user==FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa ORDER BY ponuda_poslodavca.rok_za_prijavu";
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                    }

                }
                else
                {
                    if($active!=FALSE)
                    {
                        if($active=='active')
                        {
                            if($user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND korisnik.naziv='".$user."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE && $user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location==FALSE && $user==FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                        }
                        if($active=='non_active')
                        {
                            if($user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND korisnik.naziv='".$user."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE && $user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location==FALSE && $user==FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                        }

                    }
                    else
                    {
                        if($user!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND korisnik.naziv='".$user."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location!=FALSE && $user!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location==FALSE && $user==FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                    }
            }


            }
            else
            {
                if($limit_clause==FALSE)
                {
                    if($active!=FALSE)
                    {
                        if($active=='active')
                        {
                            if($user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND korisnik.naziv='".$user."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE && $user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location==FALSE && $user==FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                        }
                        if($active=='non_active')
                        {
                            if($user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND korisnik.naziv='".$user."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE && $user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location==FALSE && $user==FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu";
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                        }

                    }
                    else
                    {
                        if($user!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND korisnik.naziv='".$user."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location!=FALSE && $user!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location==FALSE && $user==FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                    }

                }
                else
                {
                    if($active!=FALSE)
                    {
                        if($active=='active')
                        {
                            if($user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND korisnik.naziv='".$user."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE && $user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location==FALSE && $user==FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND '{$date_now}'<ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                        }
                        if($active=='non_active')
                        {
                            if($user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND korisnik.naziv='".$user."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location!=FALSE && $user!=FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                            if($location==FALSE && $user==FALSE)
                            {
                                $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                                ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND '{$date_now}'>ponuda_poslodavca.rok_za_prijavu ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                                $query = database::connection()->prepare($sql);
                                $query->execute();
                                $result=$query->fetchAll();
                            }
                        }

                    }
                    else
                    {
                        if($user!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND korisnik.naziv='".$user."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location!=FALSE && $user!=FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') AND ponuda_poslodavca.lokacija='".$location."' AND korisnik.naziv='".$user."' ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                        if($location==FALSE && $user==FALSE)
                        {
                            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                            ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa AND (naziv_tipa_aktivnosti LIKE '%".$search."%' OR lokacija LIKE '%".$search."%' OR naziv LIKE '%".$search."%') ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
                            $query = database::connection()->prepare($sql);
                            $query->execute();
                            $result=$query->fetchAll();
                        }
                    }
            }}


            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function which return diferent location of offers.
     * @return array Location of offers.
     */
    public function distinctLocation()
    {
        try
        {
            $sql="SELECT DISTINCT lokacija FROM ponuda_poslodavca";
            $query = database::connection()->prepare($sql);
            $query->execute();
            $result=$query->fetchAll();
            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    public function offersOfStudentActivity($idActivity,$limit_clause=FALSE)
    {
        if($limit_clause!=FALSE)
        {
            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                 ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=".$idActivity." AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa ORDER BY ponuda_poslodavca.rok_za_prijavu".$limit_clause;
            $query = database::connection()->prepare($sql);
            $query->execute();
            $result=$query->fetchAll();
        }
        else
        {
            $sql="SELECT * FROM ponuda_poslodavca,korisnik,sifrarnik_tipova_aktivnosti WHERE
                 ponuda_poslodavca.idKorisnika=korisnik.idKorisnika AND ponuda_poslodavca.idTipa=".$idActivity." AND ponuda_poslodavca.idTipa=sifrarnik_tipova_aktivnosti.idTipa ORDER BY ponuda_poslodavca.rok_za_prijavu";
            $query = database::connection()->prepare($sql);
            $query->execute();
            $result=$query->fetchAll();
        }

        return $result;
    }

}