<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/16/18
 * Time: 9:23 AM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'/config/database.php';
/**
 * Class for manage data in table sifrarnik_tipova_aktivnosti.
 * Class activityOfStudentsModel
 */
class activityOfStudentsModel
{
    /**
     * Function for select all data about type of students activity
     * @param bool $id id of student whose data want to see, if $id is false we will see data about all user
     * @return array|mixed Information about students activity
     */
    public function studentsActivity($id=FALSE,$limit_clause=FALSE)
    {
        try
        {
            if($id==FALSE)
            {
                $sql="SELECT * FROM sifrarnik_tipova_aktivnosti ORDER BY naziv_tipa_aktivnosti";
                $query = database::connection()->prepare($sql);
                $query->execute();
                $result=$query->fetchAll();
            }
            else
            {
                $sql="SELECT * FROM sifrarnik_tipova_aktivnosti WHERE idTipa=".$id;
                $query = database::connection()->prepare($sql);
                $query->execute();
                $result=$query->fetch();
            }
            if($limit_clause!=FALSE)
            {
                $sql="SELECT * FROM sifrarnik_tipova_aktivnosti ORDER BY naziv_tipa_aktivnosti".$limit_clause;
                $query = database::connection()->prepare($sql);
                $query->execute();
                $result=$query->fetchAll();
            }
            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function for update data about activity of students
     * @param string $data Information about activity of students
     */
    public function updateStudentActivity($data)
    {
        try
        {
            $sql="UPDATE sifrarnik_tipova_aktivnosti SET naziv_tipa_aktivnosti=:namee,opis_aktivnosti=:description WHERE idTipa=:id";
            $query = database::connection()->prepare($sql);
            $query->execute(array('namee'=>$data['n_student_activity'],'description'=>$data['d_student_activity'],':id'=>$data['id']));
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function for delete students activity from database.
     * @param string $id Id of students activity which will be deleted.
     */
    public function deleteStudentActivity($id)
    {
        try
        {
            $sql="DELETE FROM sifrarnik_tipova_aktivnosti WHERE idTipa=:id";
            $query = database::connection()->prepare($sql);
            $query->execute(array(':id'=>$id));
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function for insert student activity in database.
     * @param string $data Information about students activity which will be inserted in database.
     */
    public function insertStudentActivity($name,$description)
    {
        try
        {
            $sql="INSERT INTO sifrarnik_tipova_aktivnosti(naziv_tipa_aktivnosti,opis_aktivnosti) VALUES ('".$name."','".$description."')";
            $query = database::connection()->prepare($sql);
            $query->execute();

        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

    /**
     * Function for find some special students activity
     * @param string $data String from input field search.
     * @param bool $limit_clause Limit of result
     * @return array Students activity which are find.
     */
    public function searchStudentActivity($data,$limit_clause=FALSE)
    {
        try
        {
            if($limit_clause!=FALSE)
            {
                $sql_type="SELECT * FROM sifrarnik_tipova_aktivnosti WHERE naziv_tipa_aktivnosti LIKE '%".$data."%'".$limit_clause;
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result=$query->fetchAll();
            }
            else
            {
                $sql_type="SELECT * FROM sifrarnik_tipova_aktivnosti WHERE naziv_tipa_aktivnosti LIKE '%".$data."%'";
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result=$query->fetchAll();
            }

            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

}