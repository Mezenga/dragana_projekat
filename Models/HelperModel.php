<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/30/18
 * Time: 10:49 AM
 */

/**
 * Class with static method which will be used in application.
 * Class HelperModel
 */
class HelperModel
{
    /**
     * Function for checking request.
     * @return bool Return true if request is XHR or false if request is not XHR.
     */
    public static function isAjax()
    {
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest')
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Function which set parameters for pagination
     * @param $pg Object bootPagination class.
     * @param $pageNumber Number of page,
     * @param $pageSize Number of result on one page,.
     * @param $totalRecords Number of all result in database.
     * @param $default_url Url for default.
     * @param $pagination_url Url pagination.
     * @return mixed Return $pg which is set for using for pagination.
     */
    public static function setPagination($pg,$pageNumber,$pageSize,$totalRecords,$default_url,$pagination_url)
    {
        $pg->pagenumber = $pageNumber;
        $pg->pagesize = $pageSize;
        $pg->totalrecords = $totalRecords;
        $pg->showfirst = true;
        $pg->showlast = true;
        $pg->paginationcss = "pagination";
        $pg->paginationstyle = 1;
        $pg->defaultUrl = $default_url;
        $pg->paginationUrl = $pagination_url;
        return $pg;
    }

    public static function paginationParameters()
    {
        $and_clause = '';
        $search_term = '';
        $pageSize = 10;
        if(isset($_GET['p']))
        {
            $pageNumber = $_GET['p'];
        }
        else
        {
            $pageNumber = 1;
        }
        $limitPage = ((int)$pageNumber - 1) * $pageSize;
        $limit_clause = " LIMIT ".$limitPage.",".$pageSize;

    }
}