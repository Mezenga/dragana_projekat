<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/10/18
 * Time: 1:48 PM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'/config/database.php';
class activityModel
{
    /**
     * This function return data about activity of users(company).
     * @return array Data about activity of company
     */
    public function typeActivity($id=FALSE, $limit_clause = FALSE)
    {
        try
        {
            if($id==FALSE)
            {
                $sql_type="SELECT * FROM sifrarnik_vrste_djelatnosti ORDER BY naziv_vrste_djelatnosti";
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result_type=$query->fetchAll();
            }
            else
            {
                $sql_type="SELECT * FROM sifrarnik_vrste_djelatnosti WHERE idVrste=".$id;
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result_type=$query->fetch();
            }
            if($limit_clause!=FALSE)
            {
                $sql_type = "SELECT * FROM sifrarnik_vrste_djelatnosti ORDER BY naziv_vrste_djelatnosti". $limit_clause;
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result_type=$query->fetchAll();
            }
            return $result_type;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function which return number of activity in database
     * @return int Nmber of rows of activity
     */
    public function countActivity()
    {
        try
        {
            $sql = "SELECT * FROM sifrarnik_vrste_djelatnosti";
            $query = database::connection()->prepare($sql);
            $query->execute();
            $result=$query->fetchAll();

            return count($result);
        }
        catch(Exception $e)
        {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
     }

    /**
     * Function for insert new activity
     * @param $name Name of user activity.
     * @param $description Description of user activity.
     * @internal param string $data name and description of activity
     */
    public function addActivity($name,$description)
    {
        try
        {
            $sql="INSERT INTO sifrarnik_vrste_djelatnosti(naziv_vrste_djelatnosti,opis_djelatnosti) VALUES ('".$name."','".$description."')";
            $query = database::connection()->prepare($sql);
            $query->execute();

        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function for delete activity from database.
     * @param string $id Id of activity that needs to be deleted
     */
    public function deleteActivity($id)
    {
        try
        {
            $sql="DELETE FROM sifrarnik_vrste_djelatnosti WHERE idVrste=:id";
            $query = database::connection()->prepare($sql);
            $query->execute(array(':id'=>$id));

        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function form change information of activity of company in database.
     * @param string $data Name and description of activity from form
     * @param string $id Id of activity that needs to be updated
     */
    public function updateActivity($data,$id)
    {
        try
        {
            $sql="UPDATE sifrarnik_vrste_djelatnosti SET naziv_vrste_djelatnosti=:namee,opis_djelatnosti=:description WHERE idVrste=:id";
            $query = database::connection()->prepare($sql);
            $query->execute(array('namee'=>$data['name'],'description'=>$data['description'],':id'=>$id));
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

    /**
     * Function for find some special activities.
     * @param string $data String from search field.
     * @param bool $limit_clause Limit for result.
     * @return array Activity which is find with $data.
     */
    public function searchActivity($data,$limit_clause=FALSE)
    {
        try
        {
            if($limit_clause!=FALSE)
            {
                $sql_type="SELECT * FROM sifrarnik_vrste_djelatnosti WHERE naziv_vrste_djelatnosti LIKE '%".$data."%'".$limit_clause;
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result=$query->fetchAll();
            }
            else
            {
                $sql_type="SELECT * FROM sifrarnik_vrste_djelatnosti WHERE naziv_vrste_djelatnosti LIKE '%".$data."%'";
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result=$query->fetchAll();
            }

            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

}