<?php
/**
 * Created by Dragana Cajic.
 * User: Dragana
 * Date: 8/7/18
 * Time: 2:32 PM
 */
/**
 * Include file for access to database.
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'/config/database.php';
/**
 * Class usersModel
 * This class contains method to access the database.
 */
class usersModel
{
    /**
     * Function for access all information about users.
     * @param bool $id Id of user - by default is FALSE
     * @param bool $limit_clause
     * @param bool $reg reg is true when we want only users who have allowed acccess.
     * @return array Information about users.
     */
    public function users($id=FALSE,$limit_clause=FALSE,$reg=FALSE)
	{
        try
        {
             if($id!=FALSE)
            {
                $sql_type="SELECT idVrste FROM korisnik WHERE idKorisnika=".$id;
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result_type=$query->fetch();
                if($result_type['idVrste']==NULL)
                {
                        $sql="SELECT * FROM korisnik WHERE idKorisnika=".$id;
                }
                else
                {
                        $sql = 'SELECT * FROM korisnik,sifrarnik_vrste_djelatnosti WHERE korisnik.idKorisnika='.$id .' AND  sifrarnik_vrste_djelatnosti.idVrste=korisnik.idVrste';
                }
                $query = database::connection()->prepare($sql);
                $query->execute();
                $result=$query->fetch();
            }
            else
            {
                if($limit_clause!=FALSE)
                {
                    if($reg==FALSE)
                    {
                        $sql = "SELECT * FROM korisnik WHERE odobren_pristup='0' ORDER BY naziv".$limit_clause;

                    }
                    else
                    {
                        $sql = "SELECT * FROM korisnik WHERE idUloge='1' AND odobren_pristup='1' ORDER BY naziv".$limit_clause;

                    }
                    $query = database::connection()->prepare($sql);
                    $query->execute();
                    $result=$query->fetchAll();
                }
                else
                {
                    if($reg==FALSE)
                    {
                        $sql = "SELECT * FROM korisnik WHERE odobren_pristup='0' ORDER BY naziv";

                    }
                    else
                    {
                        $sql = "SELECT * FROM korisnik WHERE idUloge='1' AND odobren_pristup='1' ORDER BY naziv";

                    }

                    $query = database::connection()->prepare($sql);
                    $query->execute();
                    $result=$query->fetchAll();
                }

            }

            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
	}

    /**
     * Function for validate data inserted in login form.
     * @param string $data Data from login form
     * @return bool Return true if logging is correct or false if not.
     */
    public function login($data)
	{
        try
        {
            if(!empty($data['email']) || !empty($data['pswd']) )
            {
                $username = $data['email'];
                $password = $data['pswd'];

                $sql="SELECT * FROM korisnik WHERE email=:username";
                $query = database::connection()->prepare($sql);
                $query->execute(array(':username'=>$username));
                $result=$query->fetch();

                if($result && password_verify($password,$result['lozinka']) && $result['odobren_pristup']==1)
                {
                    $_SESSION["is_logged"] ="true";
                    $_SESSION["id"] =$result['idKorisnika'];
                    $_SESSION["email"]=$result['email'];
                    $_SESSION["name"] =$result['naziv'];
                    $_SESSION["role"]=$result['idUloge'];
                    $_SESSION["web_site"]=$result['web_sajt'];
                    $_SESSION["address"]=$result['kontakt_adresa'];
                    $_SESSION["description"]=$result['opis_djelatnosti_korisnika'];
                    $_SESSION["id_type"]=$result['idVrste'];
                    $_SESSION["approved_access"]=$result['odobren_pristup'];
                    $_SESSION["date_time"]=date("Y/m/d h:i:sa");

                    $sql="SELECT * FROM sifrarnik_vrste_djelatnosti WHERE idVrste=:id";
                    $query = database::connection()->prepare($sql);
                    $query->execute(array(':id'=>$_SESSION['id_type']));
                    $result_type=$query->fetch();
                    $_SESSION['name_type']=$result_type['naziv_vrste_djelatnosti'];
                    $_SESSION['description_type']=$result_type['opis_djelatnosti'];

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function for logout from application and break session.
     */
    public function logout()
	{
         try
         {
            session_destroy();
            $database=new database();
            $database->disconnect();
         }
         catch(Exception $e)
         {
             echo 'Caught exception: ',  $e->getMessage(), "\n";
         }
	}

    /**
     * Function for change information about user.
     * @param string $data email and password
     *
     */
    public function updateProfile($data)
	{
        try
        {
            $password=$data['new_pwd'];
            $id=$_SESSION['id'];
            $sql="UPDATE korisnik SET lozinka=:password WHERE idKorisnika=:id";
            $query = database::connection()->prepare($sql);
            $query->execute(array(':password'=>password_hash($password, PASSWORD_DEFAULT),':id'=>$id));

        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

	}

    /**
     * Function for get password of some user.
     * @param string $id Id of user
     * @return mixed Password of user
     */
    public function getPassword($id)
    {
        try
        {
            $sql = "SELECT lozinka FROM korisnik WHERE idKorisnika=:id";
            $query = database::connection()->prepare($sql);
            $query->execute(array(':id'=>$id));
            $result=$query->fetch();
            return $result['lozinka'];
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function for changing information about some user in database.
     * @param string $data Data which is changed in form.
     * @param string $id Id of user who want to change information.
     */
    public function changeData($data,$id)
    {
        try
        {
            $sql="UPDATE korisnik SET idVrste=:type_id,naziv=:namee,email=:email,kontakt_adresa=:address,web_sajt=:site,opis_djelatnosti_korisnika=:descr WHERE idKorisnika=:id";
            $query = database::connection()->prepare($sql);
            $query->execute(array('type_id'=>$data['activity'],'namee'=>$data['name'],'email'=>$data['change_email'],'address'=>$data['address'],'site'=>$data['web_site'],'descr'=>$data['desc'],':id'=>$id));
            if($id==$_SESSION['id'])
            {
                $_SESSION["email"]=$data['email'];
                $_SESSION["name"] =$data['name'];
                $_SESSION["web_site"]=$data['web_site'];
                $_SESSION["address"]=$data['address'];
                $_SESSION['name_type']=$data['name_type'];
                $_SESSION['description_type']=$data['description_type'];
                $_SESSION["description"]=$data['desc'];
            }

        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function for delete users from database
     * @param string $id Id of user for deleting
     */
    public function deleteUser($id)
    {
        try
        {
            $sql_ponuda="DELETE FROM ponuda_poslodavca WHERE ponuda_poslodavca.idKorisnika=:id";
            $query = database::connection()->prepare($sql_ponuda);
            $query->execute(array(':id'=>$id));
            $sql="DELETE FROM korisnik WHERE korisnik.idKorisnika=:id";
            $query = database::connection()->prepare($sql);
            $query->execute(array(':id'=>$id));

        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function for adding new user in database which is allow for admin only.
     * @param string $data Information about new user
     */
    public function addUser($data)
    {
        try
        {
            $pswd=password_hash($data['password'],PASSWORD_DEFAULT);
            $sql="INSERT INTO korisnik(idVrste,idUloge,naziv,email,lozinka,kontakt_adresa,web_sajt,odobren_pristup,opis_djelatnosti_korisnika) VALUES ('".$data['activity']."',1,'".$data['name']."','".$data['email']."','$pswd','".$data['address']."','".$data['web_site']."','0','".$data['desc']."')";
            $query = database::connection()->prepare($sql);
            $query->execute();
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function for search users.
     * @param string $data Word wich is insert for searching.
     * @return array Users who have $data in his name or email.
     */
    public function searchUser($data,$limit_clause=FALSE,$reg=FALSE)
    {
        try
        {
            if($limit_clause!=FALSE)
            {
                if($reg==FALSE)
                {
                    $sql_type="SELECT * FROM korisnik WHERE odobren_pristup='0' AND (naziv LIKE '%".$data."%' OR email LIKE '%".$data."%')".$limit_clause;

                }
                else
                {
                    $sql_type="SELECT * FROM korisnik WHERE idUloge='1' AND odobren_pristup='1' AND (naziv LIKE '%".$data."%' OR email LIKE '%".$data."%')".$limit_clause;

                }
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result=$query->fetchAll();
            }
            else
            {
                if($reg==FALSE)
                {
                     $sql_type="SELECT * FROM korisnik WHERE odobren_pristup='0' AND (naziv LIKE '%".$data."%' OR email LIKE '%".$data."%')";
                }
                else
                {
                    $sql_type="SELECT * FROM korisnik WHERE idUloge='1' AND odobren_pristup='1' AND (naziv LIKE '%".$data."%' OR email LIKE '%".$data."%')";

                }
                $query = database::connection()->prepare($sql_type);
                $query->execute();
                $result=$query->fetchAll();
            }

            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

    /**
     * Function for allowing access for user who send his registration.
     * @param string $id Id of user who will be accepted.
     */
    public function allowAccess($id)
    {

        try
        {
            $sql="UPDATE korisnik SET odobren_pristup=:access WHERE idKorisnika=:id";
            $query = database::connection()->prepare($sql);
            $query->execute(array(':access'=>1,':id'=>$id));
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

    /**
     * Function for display different names of user.
     * @return array Name of user.
     */
    public function distinctUser()
    {
        try
        {
            $sql="SELECT DISTINCT naziv FROM korisnik ORDER BY naziv";
            $query = database::connection()->prepare($sql);
            $query->execute();
            $result=$query->fetchAll();
            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Function which return users who have some activity with this id.
     * @param string $id Id of activity.
     * @param bool $limit_clause Limit of users who will be display.
     * @return array Return users with this activity.
     */
    public function userOfActivity($id,$limit_clause=FALSE)
    {
        if($limit_clause==FALSE)
        {
            $sql="SELECT * FROM korisnik WHERE idVrste=".$id;
        }
        else
        {
            $sql="SELECT * FROM korisnik WHERE idVrste=".$id.$limit_clause;
        }
        $query = database::connection()->prepare($sql);
        $query->execute();
        $result=$query->fetchAll();
        return $result;
    }

    public function adminInformation()
    {
        try
        {
            $sql="SELECT * FROM korisnik WHERE idUloge=2";
            $query = database::connection()->prepare($sql);
            $query->execute();
            $result=$query->fetch();
            return $result;
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    public function maxId()
    {
        try
        {
            $sql="SELECT MAX(idKorisnika) AS max FROM korisnik";
            $query = database::connection()->prepare($sql);
            $query->execute();
            $result=$query->fetch();
            return $result['max'];
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}

