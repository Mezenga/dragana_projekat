<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/20/18
 * Time: 3:56 PM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'Models/usersModel.php';
require_once FULL_FILE_PATH.'Models/activityModel.php';
require_once FULL_FILE_PATH.'Models/activityOfStudentsModel.php';
require_once FULL_FILE_PATH.'Models/offerModel.php';
require_once FULL_FILE_PATH.'Models/HelperModel.php';
require_once FULL_FILE_PATH.'Assets/bootstrap_pagination/pagination.php';