/**
 * Created by Dragana on 8/10/18.
 */
/**
 * Created by Dragana on 8/8/18.
 */
var error_email=false;
var error_password=false;

$('#message').hide();
$(document).ready(function(){
    var popover_user_activity=$('#popover_user_activity_content').html();
    $('#popover_user_activity_content').html("");
    $('#popover_user_activity').popover({
        title: 'Dodaj djelatnost',
        html: true,
        content: popover_user_activity
    });
    $(document).on('click','button#pop_submit_user_activity',function() {
        var form_data=$('form#popover_add_form_user_activity').serialize();
        var select_div=$('#select_user_activity');
        var message=$('div#pop_message_user_activity');
        $.ajax({
            type:'POST',
            url:'Views/validationuseractivity.php',
            data:form_data,
            dataType:'json'
        }).done(function(data){
                if(data.status==true)
                {
                    select_div.html(data.content);
                    $('#popover_user_activity').popover('hide');
                }
                else
                {
                    message.html(data.content);
                    message.attr('hidden',false);
                }
            }).fail(function(data,txtStatus,xhrObject){
                alert(txtStatus);
            });

    });

    $(document).on('click', 'button#pop_close_user_activity', function() {
        $('#popover_user_activity').popover('hide');
    });

    var popover_student_activity=$('#popover_student_activity_content').html();
    $('#popover_student_activity_content').html("");
    $('#popover_student_activity').popover({
            title: 'Dodaj aktivnost',
            html: true,
            content: popover_student_activity
    });

    $(document).on('click', 'button#pop_submit_student_activity', function() {
        var form_data=$('form#popover_add_form_student_activity').serialize();
        var select_div=$('#select_st_activity');
        var message=$('div#pop_add_message');
        $.ajax({
            type:'POST',
            url:'Views/validinsertstudentactivity.php?popover=true',
            data:form_data,
            dataType:'json'
        }).done(function(data){
                if(data.status==true)
                {
                    select_div.html(data.content);
                    $('#popover_student_activity').popover('hide');
                }
                else
                {
                    message.html(data.content);
                    message.attr('hidden',false);
                }
            }).fail(function(data,txtStatus,xhrObject){
                alert(txtStatus);
            });

    });
    $(document).on('click', 'button#pop_close_student_activity', function() {
        $('#popover_student_activity').popover('hide');
    });


    $('#registration_button').on('click',function(){
        $('#mod_registration').modal('show');
    });

    $('#message').hide();
    $('#email_error_message').hide();
    $('#password_error_message').hide();
    $('#submit_message').hide();
    $('#email').keyup(function()
    {
        check_email();
        if(error_password==false && error_email==false)
        {
            $('#click').attr('disabled',false);
        }
        else
        {
            $('#click').attr('disabled',true);
        }
    });
    $('#valid_password').keyup(function()
    {
        check_password();
        if(error_password==false && error_email==false)
        {
            $('#click').attr('disabled',false);
        }
        else
        {
            $('#click').attr('disabled',true);
        }
    });
    $('#new_password').keyup(function()
    {
        check_password();
        if(error_password==false && error_email==false)
        {
            $('#click').attr('disabled',false);
        }
        else
        {
            $('#click').attr('disabled',true);
        }
    });
    $('#btn_activity').on( "click", function() {
        $('#exampleModal').modal('show');
    });

    $('#new_activity').click(function(){
        $('#add_activity_modal').modal('show');
    });
    $('#search_users').keyup(function(){
        var search_term=$('#search_users').val();
        var div=$('#result');
        var loading=$('#loading');
        var reg=$('#reg').val();
        loading.attr('hidden',false);
        $('#clear_search_users').attr('disabled',false);
        if(search_term=="")
        {
            if(reg=="")
            {
                window.location = 'index.php?view=userslist';
            }
            else
            {
                window.location = 'index.php?view=userslist&reg='+reg;
            }

        }
        else
        {
        $.ajax({
            url:'Views/search.php?search='+search_term+'&reg='+reg,
            type:'GET',
            dataType:'text'
        }).done(function(data){
                loading.attr('hidden',true);
                div.html('');
                div.html(data);
            }).fail(function(data,txtStatus,xhrObject){
                alert(txtStatus);
            });}
    });

    $('#clear_search_users').click(function(){
        var reg=$('#reg_users').val();
        if(reg=="")
        {
            window.location = 'index.php?view=userslist';
        }
        else
        {
            window.location = 'index.php?view=userslist&reg=true';
        }
    });

    $('#search_activity').keyup(function(){
        var search_term=$('#search_activity').val();
        var div=$('#result_activity');
        var loading=$('#loading_activity');
        loading.attr('hidden',false);
        $('#clear_search_activity').attr('disabled',false);
        if(search_term=="")
        {
            window.location = 'index.php?view=activitylist';
        }
        else
        {
            $.ajax({
                url:'Views/search_activity.php?search='+search_term,
                type:'GET',
                dataType:'text'
            }).done(function(data){
                    loading.attr('hidden',true);
                    div.html('');
                    div.html(data);
                }).fail(function(data,txtStatus,xhrObject){
                    alert(txtStatus);
                });}
    });
    $('#clear_search_activity').click(function(){
        window.location = 'index.php?view=activitylist';
    });
    $('#search_studentactivity').keyup(function(){
        var search_term=$('#search_studentactivity').val();
        var div=$('#result_studentactivity');
        var loading=$('#loading_studentactivity');
        loading.attr('hidden',false);
        $('#clear_search_student_activity').attr('disabled',false);
        if(search_term=="")
        {
            window.location = 'index.php?view=studentsActivityList';
        }
        else
        {
            $.ajax({
                url:'Views/search_studentactivity.php?search='+search_term,
                type:'GET',
                dataType:'text'
            }).done(function(data){
                    loading.attr('hidden',true);
                    div.html('');
                    div.html(data);
                }).fail(function(data,txtStatus,xhrObject){
                    alert(txtStatus);
                });}
    });
    $('#clear_search_student_activity').click(function(){
        window.location = 'index.php?view=studentsActivityList';
    });
    $('#search_offers').keyup(function(){
        var user=$('#filter_user').val();
        var location=$('#filter_location').val();
        var active=$('#filter_active').val();
        var search_term=$('#search_offers').val();
        var div=$('#result_offers');
        var loading=$('#loading_offers');
        loading.attr('hidden',false);
        if(search_term=="" && user=="" && location=="" && active=="")
        {
            window.location = 'index.php?view=offerlist';
        }
        else
        {
            if(user=="" && location=="" && active=="")
            {
                $.ajax({
                    url:'Views/search_offers.php?search='+search_term,
                    type:'GET',
                    dataType:'text'
                }).done(function(data){
                        loading.attr('hidden',true);
                        div.html('');
                        div.html(data);
                    }).fail(function(data,txtStatus,xhrObject){
                        alert(txtStatus);
                    });
            }
            else
            {
                $.ajax({
                    url:'Views/filteroffers.php?user='+user+'&location='+location+'&active='+active+'&search='+search_term,
                    type:'GET',
                    dataType:'text'
                }).done(function(data){
                        loading.attr('hidden',true);
                        div.html(data);
                    }).fail(function(data,txtStatus,xhrObject){
                        alert(txtStatus);
                    });
            }

        }
    });
    $('#clear_search_offers').click(function(){
        var user=$('#filter_user').val();
        var location=$('#filter_location').val();
        var active=$('#filter_active').val();
        if(user=="" && location=="" && active=="")
        {
            window.location = 'index.php?view=offerlist';
        }
        else
        {
            window.location = 'index.php?view=offerlist&user='+user+'&location='+location+'&active='+active;
        }

    });
    $('#users').on('click',function(){
        $('#users').attr('class','active');
    });

    $('#add_sub_offer').click(function(){
        var form=$('#form_add_offer');
        var form_data=form.serialize();
        var message=$('#message_offer');
        $.ajax({
            type:'POST',
            url:'Views/validationoffers.php?offers_validation=add',
            data:form_data,
            dataType:'json'
        }).done(function(data){
                if(data.status==true)
                {
                    form.submit();
                }
                else
                {
                    message.html(data.message);
                    message.attr('hidden',false);
                }
            }).fail(function(data,txtStatus,xhrObject){
                alert(txtStatus);
            });
    });

    $('#registration').click(function(){
        var form=$('#registration_form');
        var form_dataa=form.serialize();
        var message=$('#message_reg');
        $.ajax({
            type:'POST',
            url:'Views/validationregistration.php?registration=true',
            data:form_dataa,
            dataType:'json'
        }).done(function(data){
                if(data.status==true)
                {
                    form.submit();
                }
                else
                {
                    message.html(data.message);
                    message.attr('hidden',false);
                }
            }).fail(function(data,txtStatus,xhrObject){
                alert(txtStatus);
            });
    });

    $('#edit_offer').click(function(){
        var form=$('#edit_offer_form');
        var form_data=form.serialize();
        var message=$('#message_edit_offer');
        $.ajax({
            type:'POST',
            url:'Views/validationoffers.php?offers_validation=edit',
            data:form_data,
            dataType:'json'
        }).done(function(data){
                if(data.status==true)
                {
                    form.submit();
                }
                else
                {
                    message.html(data.message);
                    message.attr('hidden',false);
                }
            }).fail(function(data,txtStatus,xhrObject){
                alert(txtStatus);
            });
    });

    $('.page-link').click(function(){
        var page=$(this);
        page.attr('class','page-link active');
    });
    var flag=0;

    $('.loadoffers').click(function()
    {
        var id=$(this).attr('id');
        var div=$('#result_offers_of_user');
        var hidden=$('#hidden').val();
        var more=$('.loadmoreoffers');
        if(hidden=='true')
        {
            more.attr('hidden',true);
        }
        else
        {
            more.attr('hidden',false);
        }
        $('#row').attr('hidden',false);
        $.ajax({
            url:'Views/offersofuser.php',
            type:'POST',
            data:{id:id,limit:10,start:0,first:true},
            dataType:'text'
        }).done(function(data){
                div.html(data);

                flag+=10;
            }).fail(function(data,txtStatus,xhrObject){
                alert(txtStatus);
            });
    });
    $('.loadmoreoffers').click(function()
    {
        var id=$(this).attr('id');
        var div=$('#result_offers_of_user');
        var more=$('.loadmoreoffers');
        $.ajax({
            url:'Views/offersofuser.php',
            type:'POST',
            data:{id:id,limit:10,start:flag},
            dataType:'json'
        }).done(function(data){
                div.append(data.content);
                if(data.btn_hidden=='true')
                {
                    more.attr('hidden','true');
                }

                flag+=10;
            }).fail(function(data,txtStatus,xhrObject){
                alert(txtStatus);
            });
    });

    $('.select-filter').on('change',function()
    {
        var search_term=$('#search_offers').val();
        var user=$('#filter_user').val();
        var location=$('#filter_location').val();
        var active=$('#filter_active').val();
        var div=$('#result_offers');
        var loading=$('#loading_');
        loading.attr('hidden',false);
        if(user!="" || location!="" || active!="")
        {
            $('#clear_filter').attr('disabled',false);
        }
        else
        {
            $('#clear_filter').attr('disabled',true);
        }
        $.ajax({
            url:'Views/filteroffers.php?user='+user+'&location='+location+'&active='+active+'&search='+search_term,
            type:'GET',
            dataType:'text'
        }).done(function(data){
                loading.attr('hidden',true);
                div.html(data);
            }).fail(function(data,txtStatus,xhrObject){
                alert(txtStatus);
            });
    });
    $('#search_offers').on('keyup',function(){
        $('#clear_search_offers').attr('disabled',false);
    });
    $('#clear_filter').click(function(){
        var search_term=$('#search_offers').val();
        if(search_term!="")
        {
            window.location = 'index.php?view=offerlist&search='+search_term;
        }
        else
        {
            window.location = 'index.php?view=offerlist';
        }

    });
    $('#all_offers').on('click',function(){
        window.location = 'index.php?view=offerlist';
    });
    var date_deadline_check_in=$('#deadline_check_in');
    var date_start_offer=$('#start');
    date_deadline_check_in.datepicker({
        dateFormat: "yy-mm-dd"
    });
    date_start_offer.datepicker({
        dateFormat: "yy-mm-dd"
    });
    $('#date_filter').datepicker({
        dateFormat: "yy-mm-dd"
    });
});

function CheckOldPassword()
{
    var password=$('#old_pswd').val();
    var form=$('#validation-form');
    var form_data=form.serialize();
    var message=$('#message');
    $.ajax({
        type:'POST',
        url:'/dragana_projekat/Views/editprofile.php?pswd=true',
        data: form_data,
        dataType:'json'
        }).done(function(data){
            if(data.status==true)
            {
                form.submit();
            }
            else
            {
                message.html(data.message);
                message.attr('hidden',false);
            }
        }).fail(function(data,txtStatus,xhrObject)
        {
            alert(txtStatus);
        });
}
function CheckChangeData(id)
{
    var result=true;
    var pattern=new RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/);
    var message_data=$('#message_data');
    if($('#name_edit_data').val()=="")
    {
        message_data.html("Unesite naziv!");
        message_data.attr('hidden',false);
        result=false;
    }
    if($('#change_email').val()=="")
    {
        message_data.html("Unesite email!");
        message_data.attr('hidden',false);
        result=false;
    }

    if(!pattern.test($('#change_email').val()))
    {
        message_data.html("Pogrešan format za email!");
        message_data.attr('hidden',false);
        result=false;
    }
    if($('#address').val()=="")
    {
        message_data.html("Unesite adresu!").show();
        message_data.attr('hidden',false);
        result=false;
    }
    if($('#web_site').val()=="")
    {
        message_data.html("Unesite web sajt!").show();
        message_data.attr('hidden',false);
        result=false;
    }
    if($('#desc').val()=="")
    {
        message_data.html("Unesite opis djelatnosti!").show();
        message_data.attr('hidden',false);
        result=false;
    }
    return result;
}
function EditStudentActivity(id)
{
    var modal_d=$('#studentsActivity');
    $.ajax({
        url:'Views/editstudentactivity.php',
        method:'post',
        data:{id_edit:id}
    }).done(function(data){
            $('#edit_modal-body').html(data);
            modal_d.modal("show");
        });
}
function EditOffer(id)
{
    var modal_d=$('#offer');
    $.ajax({
        url:'Views/editoffer.php',
        method:'post',
        data:{id_edit:id}
    }).done(function(data){
            $('#offer-modal-body').html(data);
            $('#deadline_check_in').datepicker({
                dateFormat: "yy-mm-dd"
            });
            $('#start').datepicker({
                dateFormat: "yy-mm-dd"
            });
            modal_d.modal("show");
        });
}
function ValidationAddStudentActivity()
{
    var form=$('#add_form_student_activity');
    var form_data=form.serialize();
    var message=$('#add_message');
    $.ajax({
        type:'POST',
        url:'Views/validinsertstudentactivity.php?add_student_activity=true',
        data:form_data,
        dataType:'json'
    }).done(function(data){
            if(data.status==true)
            {
                form.submit();
            }
            else
            {
                message.html(data.message);
                message.attr('hidden',false);
            }
        }).fail(function(data,txtStatus,xhrObject){
            alert(txtStatus);
        });
}

function Delete(id)
{
    var reg=$('#reg').val();
    var r = confirm("Brisanjem ovog korisnika brisete i sve njegove ponude! Da li ste sigurni da želite obrisati ovog korisnika?");
    if (r == true)
    {
        window.location = 'index.php?view=deleteuser&id='+id+'&reg='+reg;
    }
}

function DeleteActivity(id)
{
    var r = confirm("Da li ste sigurni da želite obrisati djelatnost?");

    if (r == true)
    {
        window.location = 'index.php?view=deleteactivity&id='+id;
    }
}

function DeleteStudentActivity(id)
{
    var r = confirm("Da li ste sigurni da želite obrisati ovu aktivnost?");

    if (r == true)
    {
        window.location = 'index.php?view=deletestudentactivity&id='+id;
    }
}

function DeleteOffer(id)
{
    var r = confirm("Da li ste sigurni da želite obrisati ovu ponudu?");

    if (r == true)
    {
        window.location = 'index.php?view=deleteoffer&id='+id;
    }
}

function check_email()
{
    var pattern=new RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/);
    if(pattern.test($('#email').val()))
    {
        $('#email_error_message').hide();
        error_email=false;
    }
    else
    {
        $('#email_error_message').html("Pogresna email adresa!");
        $('#email_error_message').show();
        error_email=true;
    }
}

function check_password()
{
    var password=$('#new_password').val();
    var retype_password=$('#valid_password').val();
    if(password!=retype_password)
    {
        $('#password_error_message').html("Lozinke se ne poklapaju!");
        $('#password_error_message').show();
        error_password=true;
    }
    else
    {
        $('#password_error_message').hide();
        error_password=false;
    }
}
function checkBlankEditStudentActivity()
{
    var message=$('#edit_student_activity_message');
    var result=true;
    if($('#name_type').val()=="")
    {
        message.html("Unesite naziv!");
        message.attr('hidden',false);
        result=false;
    }
    return result;
}
function checkEditUserActivity()
{
    var message=$('#edit_user_activity_message');
    var result=true;
    if($('#name_edit_user_activity').val()=="")
    {
        message.html("Unesite naziv!");
        message.attr('hidden',false);
        result=false;
    }
    if($('#description_edit_user_activity').val()=="")
    {
        message.html("Unesite opis!");
        message.attr('hidden',false);
        result=false;
    }

    return result;
}
function checkBlank()
{
    var result=true;
    var pattern=new RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/);
    var message=$('#message_user');
    if($('#name_user').val()=="")
    {
        message.html("Unesite naziv!");
        message.attr('hidden',false);
        result=false;
    }
    if($('#pwd_user').val()=="")
    {
        message.html("Unesite lozinku!").show();
        message.attr('hidden',false);
        result=false;
    }
    if($('#email_user').val()=="")
    {
        message.html("Unesite email!").show();
        message.attr('hidden',false);
        result=false;
    }
    else
    {
        if(!pattern.test($('#email_user').val()))
        {
            message.html("Pogrešan format za email!").show();
            message.attr('hidden',false);
            result=false;
        }
    }
    if($('#address_user').val()=="")
    {
        message.html("Unesite adresu!").show();
        message.attr('hidden',false);
        result=false;
    }
    if($('#web_site_user').val()=="")
    {
        message.html("Unesite web site!").show();
        message.attr('hidden',false);
        result=false;
    }
    if($('#desc_user').val()=="")
    {
        message.html("Unesite opis!").show();
        message.attr('hidden',false);
        result=false;
    }
    if($('#name_type').val()=="")
    {
        $('#edit_student_activity_message').html("Unesite naziv djelatnosti!").show();
        $('#edit_student_activity_message').attr('hidden',false);
        result=false;
    }
    return result;

}
function checkBlankAddActivity()
{
    var result=true;
    var message=$('#message_activity');
    if($('#name_activity').val()=="")
    {
        message.html("Unesite naziv djelatnosti!");
        message.attr('hidden',false);
        result=false;
    }
    if($('#description_type').val()=="")
    {
        message.html("Unesite opis djelatnosti!").show();
        message.attr('hidden',false);
        result=false;
    }
    return result;
}












