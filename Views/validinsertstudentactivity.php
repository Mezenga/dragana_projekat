<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/16/18
 * Time: 2:36 PM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
$activityStudentModel=new activityOfStudentsModel();
$students_activity=$activityStudentModel->studentsActivity();
$exist=0;
if(HelperModel::isAjax()==true)
{
    //validation input field of form for adding student activity
    if(isset($_REQUEST['add_student_activity']))
    {
        header('Content-Type: application/json');
        //input field for name and description of student activity must be fill
        if($_POST['add_name_student_activity']!="" && $_POST['add_description_student_activity']!="")
        {
            foreach($students_activity as $stud_activity)
            {
                //check if name of student activity already exist in database
                if($_POST['add_name_student_activity']==$stud_activity['naziv_tipa_aktivnosti'])
                {
                    $exist++;
                    print json_encode(array('status' => false,'message'=>'Aktivnost vec postoji u bazi!'));
                    die();
                }
            }
            //return true if both condition are ok
            if($exist==0)
            {
                print json_encode(array('status' => true));
            }
        }
        else
        {
            print json_encode(array('status' => false,'message'=>'Popunite sva polja!'));
        }
    }
    //validation input field of form for adding student activity with popover which is inside modal for adding offer
    if(isset($_REQUEST['popover']))
    {
        //input field for name and description of student activity must be fill
        if($_POST['pop_add_name_student_activity']!="" && $_POST['pop_add_description_student_activity']!="")
        {
            foreach($students_activity as $stud_activity)
            {
                //check if name of student activity already exist in database
                if($_POST['pop_add_name_student_activity']==$stud_activity['naziv_tipa_aktivnosti'])
                {
                    $exist++;
                    print json_encode(array('status' => false,'content'=>'Aktivnost vec postoji u bazi!'));
                    die();
                }
            }
            //if both condition are ok insert that activity in database
            if($exist==0)
            {
                $activityStudentModel->insertStudentActivity($_POST['pop_add_name_student_activity'],$_POST['pop_add_description_student_activity']);
                $stud_activity=$activityStudentModel->studentsActivity();
                $data=" <select class='custom-select' name='add_offer_type'>";
                foreach($stud_activity as $activity)
                {
                    if($activity['naziv_tipa_aktivnosti']==$_POST['pop_add_name_student_activity'])
                    {
                        $data.="<option value=".$activity['idTipa'].">".$activity['naziv_tipa_aktivnosti']."</option>";
                    }
                }
                foreach($stud_activity as $activity)
                {
                    if($activity['naziv_tipa_aktivnosti']!=$_POST['pop_add_name_student_activity'])
                    {
                        $data.="<option value=".$activity['idTipa'].">".$activity['naziv_tipa_aktivnosti']."</option>";
                    }
                }
                $data.="</select>";
                print json_encode(array('status' => true,'content'=>$data));
            }
        }
        else
        {
            print json_encode(array('status' => false,'content'=>'Popunite sva polja!'));
        }
    }
}
