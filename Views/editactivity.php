<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/10/18
 * Time: 3:14 PM
 */
require_once 'config/loader_models.php';
$activityModel=new activityModel();
$result_id=$activityModel->typeActivity($_GET['id']);
if(!empty($result_id))
{
    $result=$activityModel->typeActivity($_GET['id']);
}
else
{
    header('Location:index.php?view=withoutview');
}
if(isset($_POST['submit_activity']))
{
    $activityModel->updateActivity($_POST,$_GET['id']);
    header('Location:index.php?view=activitylist');
}
?>

<div class="container">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <form action="" method="post" onsubmit="return checkEditUserActivity();" autocomplete="off">
                <div class="alert alert-warning" role="alert" id="edit_user_activity_message" hidden="true"></div>
                <div class="form-group">
                    <label for="name">Naziv:</label>
                    <input type="text" class="form-control" id="name_edit_user_activity" name="name" value="<?= $result['naziv_vrste_djelatnosti'];?>">
                </div>
                <div class="form-group">
                    <label for="description">Opis:</label>
                    <input type="text" class="form-control" id="description_edit_user_activity" name="description" value="<?= $result['opis_djelatnosti'];?>">
                </div>
                <div class="form-group">
                    <button type="submit" name="submit_activity" class="btn btn-primary">Potvrdi</button>
                </div>
            </form>
        </div>
        <div class="col-3"></div>
    </div>
</div>