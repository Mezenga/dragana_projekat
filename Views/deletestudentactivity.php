<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/16/18
 * Time: 1:12 PM
 */
require_once 'config/loader_models.php';
$activity_student_Model=new activityOfStudentsModel();
$offerModel=new offerModel();
$exist=0;
if(isset($_GET['id']))
{
    $result=$activity_student_Model->studentsActivity($_GET['id']);
    $offers=$offerModel->offerList();
    foreach($offers as $offer)
    {
        if($offer['idTipa']==$_GET['id'])
        {
            $exist=1;
        }
    }
    if($exist==0)
    {
        if(!empty($result))
        {
            $activity_student_Model->deleteStudentActivity($_GET['id']);
            header('Location:index.php?view=studentsActivityList');
        }
        else
        {
            header('Location:index.php?view=withoutview');
        }
    }
    else
    {
        echo '<div class="alert alert-warning" role="alert">Nije moguće obrisati aktivnost za koju postoji ponuda!</div>';
    }
}