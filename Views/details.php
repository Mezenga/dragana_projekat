<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/8/18
 * Time: 2:28 PM
 */

if(isset($_GET['id']))
{
    $result=$usersModel->users($_GET['id']);
    if(empty($result))
    {
       header('Location:index.php?view=withoutview');
    }
    $offerModel=new offerModel();
    $number=count($offerModel->offersOfUser($_GET['id']));
    if($number>=10):
        ?>
        <input id="hidden" hidden="true" value="false">
    <?php else:
    ?>
    <input hidden="true" id="hidden" value="true">
   <?php endif; }
    ?>
<div class="container">
    <div class="row bg-light">
        <div class="col text-center">
           <h2><?= $result['naziv']; ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col text-center">
            <?php   if($result['odobren_pristup']=='0'):?>
               Pristup nije odobren!
            <?php else: ?>
                Odobren pristup!
            <?php endif;?>
        </div>
    </div>
    <br>
<div class="row">
    <div class="col-6 border bg-light">
        Email:
    </div>
    <div class="col-6 border bg-light">
        Kontakt adresa:
    </div>
</div>
    <div class="row">
        <div class="col-6 border">
            <?= $result['email']; ?>
        </div>
        <div class="col-6 border">
            <?= $result['kontakt_adresa']; ?>
        </div>
      </div>
    <br>
    <div class="row">
        <div class="col-6 bg-light border">
            Web sajt:
        </div>
        <div class="col-6 bg-light border">
            Vrsta djelatnosti:
        </div>

    </div>
    <div class="row">
        <div class="col-6 border">
            <?= $result['web_sajt']; ?>
        </div>
        <div class="col-6 border">
            <?= $result['naziv_vrste_djelatnosti']; ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12 float-left border bg-light">
            Opis djelatnosti:
        </div>
    </div>
    <div class="row">
        <div class="col-12 float-left border">
            <?= $result['opis_djelatnosti_korisnika']; ?>
        </div>
    </div>
<?php
if($result['odobren_pristup']==1):
    ?>
    <br>
    <br>
        <div class="row" hidden="true" id="row">
            <div class="col border bg-light text-center">Redni broj</div>
            <div class="col border bg-light text-center">Tip ponude</div>
            <div class="col border bg-light text-center">Ciklus studija</div>
            <div class="col border bg-light text-center">Rok za prijavu</div>
            <div class="col border bg-light text-center">Maksimalan broj kandidata</div>
            <div class="col border bg-light text-center">Trajanje</div>
        </div>
    <div id="result_offers_of_user">

        <div class="row">
            <div class="col-10">

            </div>
            <div class="col-2">
                <?php if($number!=0): ?>
                <button type="button" class="btn btn-primary float-right loadoffers" id="<?= $result['idKorisnika']; ?>">Ucitaj ponude</button>
    <?php else: ?>
                    <div class="alert alert-warning" role="alert">Nema ponuda!</div>
             <?php   endif; ?>
            </div>
        </div>
    </div>
        <br>
    <div class="row">
        <div class="col float-right">
            <button type="button" class="btn btn-primary float-right loadmoreoffers" id="<?=$result['idKorisnika']; ?>" hidden="true">Ucitaj jos ponuda</button>
        </div>
    </div>
<?php
else: ?>
    <div>
        <br>
    <a class="btn btn-primary float-right" href="<?=FULL_URL_PATH;?>index.php?view=allowaccess&access=true&id=<?=$_GET['id'];?>" role="button">Dozvoli registraciju</a>
        </div>
<?php endif;
?>
