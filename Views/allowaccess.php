<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/23/18
 * Time: 2:46 PM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
$usersModel=new usersModel();

if(isset($_GET['access']))
{
    $user=$usersModel->users($_GET['id']);
    $usersModel->allowAccess($_GET['id']);
    $mail=$user['email'];
    $link='<a href='.FULL_URL_PATH.'index.php?login=true'.'>Login</a>';
    $headers = "Content-type: text/html\r\n";
    $msg = "Postovani,<br><br>Vasa registracija je odobrena!<br>Sada mozete da objavljujete konkurse, nakon logovanja sa vasom mail adresom i lozinkom!<br>Ulogujte se na sledecem linku: ".$link."<br>
    Radujemo se saradnji!<br><br>Pozdrav,<br>Elektrotehnicki fakultet Istocno Sarajevo";
    if(!mail($mail,"Odobrena registracija!",$msg,$headers))
    {
      echo 'error';
    }
    header('location:index.php?view=userslist');
}