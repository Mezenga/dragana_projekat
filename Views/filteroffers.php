<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/27/18
 * Time: 2:03 PM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'\dragana_projekat\config\loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
$offerModel=new offerModel();
$user=$_GET['user'];
$location=$_GET['location'];
$active=$_GET['active'];
$search=$_GET['search'];
$and_clause = '';
$default_url = 'index.php?view=offerlist';
$pagination_url = 'index.php?view=offerlist&p=[p]&user='.$_GET['user'].'&location='.$_GET['location'].'&active='.$_GET['active'].'&search='.$_GET['search'];
$search_term = '';
$pageSize = 10;
//set number of page on 1 or on parameter p if it is set
if(isset($_GET['p']))
{
    $pageNumber = $_GET['p'];
}
else
{
    $pageNumber = 1;
}
$limitPage = ((int)$pageNumber - 1) * $pageSize;
$limit_clause = " LIMIT ".$limitPage.",".$pageSize;

$result=$offerModel->offerList();
//result of offers if search input is empty
//inside there are different variants of filters
if($search=="")
{
    if($active=="")
    {
        if($user!="")
        {
            $offers=$offerModel->filterOffer($user,FALSE,FALSE,$limit_clause);
            $result=$offerModel->filterOffer($user);
        }
        if($location!="")
        {
            $offers=$offerModel->filterOffer(FALSE,$location,FALSE,$limit_clause);
            $result=$offerModel->filterOffer(FALSE,$location,FALSE,FALSE);
        }
        if($location!="" && $user!="")
        {
            $offers=$offerModel->filterOffer($user,$location,FALSE,$limit_clause);
            $result=$offerModel->filterOffer($user,$location);
        }
        if($location=="" && $user=="")
        {
            $offers=$offerModel->filterOffer(FALSE,FALSE,FALSE,$limit_clause,FALSE);
            $result=$offerModel->filterOffer(FALSE,FALSE,FALSE);
        }
    }
    else
    {
        if($user!="")
        {
            $offers=$offerModel->filterOffer($user,FALSE,$active,$limit_clause);
            $result=$offerModel->filterOffer($user,FALSE,$active,FALSE);
        }
        if($location!="")
        {
            $offers=$offerModel->filterOffer(FALSE,$location,$active,$limit_clause);
            $result=$offerModel->filterOffer(FALSE,$location,$active,FALSE);
        }
        if($location!="" && $user!="")
        {
            $offers=$offerModel->filterOffer($user,$location,$active,$limit_clause);
            $result=$offerModel->filterOffer($user,$location,$active,FALSE);
        }
        if($location=="" && $user=="")
        {
            $offers=$offerModel->filterOffer(FALSE,FALSE,$active,$limit_clause);
            $result=$offerModel->filterOffer(FALSE,FALSE,$active,FALSE);
        }
    }
    if($active=="" && $user=="" && $location=="")
    {
        $offers=$offerModel->filterOffer(FALSE,FALSE,FALSE,$limit_clause,FALSE);
        $result=$offerModel->filterOffer(FALSE,FALSE,FALSE,FALSE,FALSE);
    }
}
//result of offers if search input is not empty
//inside there are different variants of filters
else
{
    if($active=="")
    {
        if($user!="")
        {
            $offers=$offerModel->filterOffer($user,FALSE,FALSE,$limit_clause,$search);
            $result=$offerModel->filterOffer($user,FALSE,FALSE,FALSE,$search);
        }
        if($location!="")
        {
            $offers=$offerModel->filterOffer(FALSE,$location,FALSE,$limit_clause,$search);
            $result=$offerModel->filterOffer(FALSE,$location,FALSE,FALSE,$search);
        }
        if($location!="" && $user!="")
        {
            $offers=$offerModel->filterOffer($user,$location,FALSE,$limit_clause,$search);
            $result=$offerModel->filterOffer($user,$location,FALSE,FALSE,$search);
        }
        if($location=="" && $user=="")
        {
            $offers=$offerModel->filterOffer(FALSE,FALSE,FALSE,$limit_clause,$search);
            $result=$offerModel->filterOffer(FALSE,FALSE,FALSE,FALSE,$search);
        }

    }
    else
    {
        if($user!="")
        {
            $offers=$offerModel->filterOffer($user,FALSE,$active,$limit_clause,$search);
            $result=$offerModel->filterOffer($user,FALSE,$active,FALSE,$search);
        }
        if($location!="")
        {
            $offers=$offerModel->filterOffer(FALSE,$location,$active,$limit_clause,$search);
            $result=$offerModel->filterOffer(FALSE,$location,$active,FALSE,$search);
        }
        if($location!="" && $user!="")
        {
            $offers=$offerModel->filterOffer($user,$location,$active,$limit_clause,$search);
            $result=$offerModel->filterOffer($user,$location,$active,FALSE,$search);
        }
        if($location=="" && $user=="")
        {
            $offers=$offerModel->filterOffer(FALSE,FALSE,$active,$limit_clause,$search);
            $result=$offerModel->filterOffer(FALSE,FALSE,$active,FALSE,$search);
        }
    }
    if($active=="" && $user=="" && $location=="")
    {
        $offers=$offerModel->filterOffer(FALSE,FALSE,FALSE,$limit_clause,$search);
        $result=$offerModel->filterOffer(FALSE,FALSE,FALSE,FALSE,$search);
    }
}
$totalRecords = count($result);
$pg = new bootPagination();
$pg=HelperModel::setPagination($pg,$pageNumber,$pageSize,$totalRecords,$default_url,$pagination_url);
if(!empty($offers))
{
    ?>
    <div class="text-center" id="loading_filter_offers" hidden="true">
        <img src="<?= FULL_URL_PATH;?>Assets/icons/blue-loading-gif-transparent-9.gif">
    </div>
  <?php  include FULL_FILE_PATH."Views/tables/table_offerlist.php";
}
else
{ ?>
    <div class="alert alert-warning" role="alert">Nema rezultata!</div>
<?php }

