<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/22/18
 * Time: 2:06 PM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
$offerModel=new offerModel();
if(isset($_GET['id']))
{
    $offer=$offerModel->offerList($_GET['id']);
    if(empty($offer))
    {
        header('Location:index.php?view=withoutview');
    }
}
?>
<div class="container">
    <table class="table table-bordered table-hover container">
        <thead class="text-center table-primary">
        <tr class="table-active">
            <th>Aktivnost</th>
            <th>Poslodavac</th>
            <th>Ciklus studija</th>
            <th>Rok za prijavu</th>
            <th>Maksimalan broj kandidata</th>
            <th>Lokacija</th>
            <th>Pocetak</th>
            <th>Trajanje</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><?= $offer['naziv_tipa_aktivnosti'];?></td>
                <?php if(isset($_SESSION['is_logged']) && $_SESSION['role']==2): ?>
                <td><a href=<?=FULL_URL_PATH;?>index.php?view=details&id=<?=$offer['idKorisnika'];?>><?= $offer['naziv'];?></a></td>
                <?php else: ?>
                <td><?= $offer['naziv'];?></td>
                <?php endif; ?>
                <td><?= $offer['ciklus_studija'];?></td>
                <td><?= $offer['rok_za_prijavu'];?></td>
                <td><?= $offer['maksimalan_broj_kandidata'];?></td>
                <td><?= $offer['lokacija'];?></td>
                <td><?= $offer['pocetak'];?></td>
                <td><?= $offer['trajanje'];?></td>

            </tr>
        </tbody>
    </table>
<table class="table table-bordered table-hover container">
    <thead class="text-center table-primary">
    <tr class="table-active">
        <th>Opis ponude</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= $offer['opis_ponude'];?></td>
    </tr>
    </tbody>
</table>


    <div class="float-right">
        <button type="button" class="btn btn-primary float-right" id="all_offers" data-toggle="modal" data-target="#mod_registration">
            Sve ponude
        </button>
    </div>
</div>
