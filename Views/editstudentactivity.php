<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/16/18
 * Time: 9:34 AM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
$activityStudentsModel=new activityOfStudentsModel();
if(isset($_POST['id_edit']))
{
    $studentactivity=$activityStudentsModel->studentsActivity($_POST['id_edit']);
?>
<div class="form-group">
    <input type="text" hidden="true" value="<?= $_POST['id_edit'];?>" name="id">

</div>
    <div class="alert alert-warning" role="alert" id="edit_student_activity_message" hidden="true"></div>
<div class="form-group">
    <label for="name">Naziv aktivnosti:</label>
    <input type="text" title="Obavezan unos!" value="<?= $studentactivity['naziv_tipa_aktivnosti']; ?>" class="form-control" id="name_type" name="n_student_activity">
</div>
<div class="form-group">
    <label for="description">Opis aktivnosti:</label>
    <input type="text" class="form-control" value="<?= $studentactivity['opis_aktivnosti']; ?>" name="d_student_activity" id="description_type">
</div>
<?php } ?>