<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/10/18
 * Time: 1:38 PM
 */
$and_clause = '';
$default_url = 'index.php?view=activitylist';
$pagination_url = 'index.php?view=activitylist&p=[p]';
$search_term = '';
$pageSize = 10;
$activityModel=new activityModel();
$result=$activityModel->typeActivity();
//for adding activity from modal
if(isset($_POST['submit_activity']))
{
    $activityModel->addActivity($_POST['name_type'],$_POST['description_type']);
}
//set number of page on 1  or if is set parameter p in url on value of p
if(isset($_GET['p']))
{
    $pageNumber = $_GET['p'];
}
else
{
    $pageNumber = 1;
}
$limitPage = ((int)$pageNumber - 1) * $pageSize;
$limit_clause = " LIMIT ".$limitPage.",".$pageSize;
$activity=$activityModel->typeActivity(FALSE,$limit_clause);
//result of activity if search input is not empty
if(isset($_GET['search']))
{
    $result=$activityModel->searchActivity($_GET['search']);
    $activity=$activityModel->searchActivity($_GET['search'],$limit_clause);
    $pagination_url = 'index.php?view=activitylist&p=[p]&search='.$_GET['search'];
    $default_url = 'index.php?view=activitylist&search='.$_GET['search'];
}
//result of activity if search is empty and we want to see all users
else
{
    $activity=$activityModel->typeActivity(FALSE,$limit_clause);
}
$totalRecords = count($result);
$pg = new bootPagination();
$pg=HelperModel::setPagination($pg,$pageNumber,$pageSize,$totalRecords,$default_url,$pagination_url);
?>
<div class="container">
    </br>
    <div class="row">
        <div class="col-6">
            <?php
                if($_SESSION['role']=='2'):
            ?>
                <button type="button" class="btn btn-primary float-left" data-toggle="modal" data-target="#add_activity_modal" id="new_activity">
                    Dodaj djelatnost
                </button>
            <?php
                endif;
            ?>
        </div>
        <div class="col-2">
            <img class="float-right" style="width: 20px;height: 30px;" src="<?= FULL_URL_PATH;?>Assets/icons/search.png">
        </div>
        <div class="col-3">
            <?php if (isset($_GET['search'])):?>
                <input class="float-right form-control" type="search" placeholder="Pretraga..." id="search_activity" value=<?= $_GET['search'];?>>
            <?php else:  ?>
                <input class="float-right form-control" type="search" placeholder="Pretraga..." id="search_activity">
            <?php endif; ?>
        </div>
        <div class="col-1">
            <button type="button" class="btn btn-primary float-right" id="clear_search_activity" disabled="true">Ponisti</button>
        </div>
    </div>
    </br>
    <div id="result_activity">
        <?php
            include FULL_FILE_PATH."/Views/tables/table_activitylist.php";
        ?>
    </div>
</div>
<?php
include FULL_FILE_PATH."/Views/modals/addactivity.php";
?>
