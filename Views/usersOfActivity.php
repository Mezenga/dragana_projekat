<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/10/18
 * Time: 4:25 PM
 */
require_once FULL_FILE_PATH.'config/loader_models.php';
$default_url = 'index.php?view=usersOfActivity&id='.$_GET['id'];
$pagination_url = 'index.php?view=usersOfActivity&p=[p]&id='.$_GET['id'];
$and_clause = '';
$search_term = '';
$pageSize = 10;
if(isset($_GET['p']))
{
    $pageNumber = $_GET['p'];
}
else
{
    $pageNumber = 1;
}
$limitPage = ((int)$pageNumber - 1) * $pageSize;
$limit_clause = " LIMIT ".$limitPage.",".$pageSize;
$activityModel=new activityModel();
$activity=$activityModel->typeActivity($_GET['id']);
if(empty($activity))
{
    header('Location:index.php?view=withoutview');
}
$usersModel=new usersModel();
$result=$usersModel->userOfActivity($_GET['id']);
$users=$usersModel->userOfActivity($_GET['id'],$limit_clause);
$totalRecords = count($result);
$pg = new bootPagination();
$pg=HelperModel::setPagination($pg,$pageNumber,$pageSize,$totalRecords,$default_url,$pagination_url);
if(!empty($users)):
?>
<div class="row">
    <div class="col-3">
    </div>
    <div class="col-6">
        <table class="table table-bordered table-hover">
            <thead class="text-center table-active">
                <tr class="table-active">
                    <th>Redni broj</th>
                    <th><?=$activity['naziv_vrste_djelatnosti']?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(isset($_GET['p']))
                {
                    $number=($_GET['p']-1)*10+1;
                }
                else
                {
                    $number=1;
                }
                foreach($users as $user):
                ?>
                <tr>
                    <td class='text-center table-active'><?= $number;?>.</td>
                    <td><a href=<?=FULL_URL_PATH;?>index.php?view=details&id=<?=$user['idKorisnika'];?>><?=$user['naziv'];?></a></td>
                 </tr>
                <?php
                    $number++;
                endforeach;
                ?>
            </tbody>
        </table>
    </div>
    <div class="col-3">
    </div>
</div>
<div class="row">
    <div class="col-3">
    </div>
    <div class="col-3 ">
    <?php
    echo $pg->process();
    ?>
    </div>
    <div class="col-3 float-right">
        <span class="float-right text-primary"> Ukupno:&nbsp;<?=$pg->totalrecords ?></span>
    </div>
    <div class="col-3">
    </div>
</div>
<?php else: ?>
<div class="alert alert-warning" role="alert">Nema korisnika za djelatnost <?php print_r($activity['naziv_vrste_djelatnosti']); ?>!</div>
<?php endif; ?>