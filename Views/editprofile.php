<?php

/**
 * Created by Dragana Cajic.
 * User: Dragana
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
$usersModel=new usersModel();
if(isset($_REQUEST['pswd']))
{
    $old=$usersModel->getPassword($_SESSION["id"]) ;
    header('Content-Type: application/json');
    if(password_verify($_REQUEST['old_password'],$old))
    {
        print json_encode(array('status' => true, 'message' => 'Password match'));
    }
    else
    {
        print json_encode(array('status' => false, 'message' => 'Password match'));
    }
die();
}
if(isset($_POST['email']))
{
    $usersModel->updateProfile($_POST);
}
?>
<div class="modal fade" id="change_profile">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Promjena lozinke</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

      <form action="#" method="POST" id="validation-form" name="profile">
          <div class="modal-body">
          <div id="message" class="alert alert-warning container"></div>
          <div class="form-group">
              <label for="pwd">Stara lozinka:</label>
              <input type="password" class="form-control border border-danger" id="old_pswd" name="old_password" placeholder="Unesite staru lozinku">
          </div>
        <div  class="alert alert-warning container" id='email_error_message'></div>

        <div class="form-group">
            <label for="pwd">Lozinka:</label>
            <input type="password" class="form-control" id="new_password" name="new_pwd" placeholder="Unesite novu lozinku">
        </div>
        <div class="form-group">
            <label for="pwd">Potvrda lozinke:</label>
            <input type="password" class="form-control" id="valid_password" name="Valid_pswd" placeholder="Ponovite novu lozinku">
        </div>
        <div  class="alert alert-warning container" id='password_error_message'></div>
        <div  class="alert alert-warning container" id='submit_message'><strong>Nemoguca izmjena!</strong></div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-primary" id="click" name="potvrdi" onclick="CheckOldPassword();" >Sacuvaj</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
          </div>
      </form>
        </div>
    </div>
</div>

