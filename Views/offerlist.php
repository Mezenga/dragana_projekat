<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/22/18
 * Time: 8:37 AM
 */
$and_clause = '';
$default_url = 'index.php?view=offerlist';
$pagination_url = 'index.php?view=offerlist&p=[p]';
$search_term = '';
$pageSize = 10;
$offerModel=new offerModel();
$userModel=new usersModel();
$activityStudentsModel=new activityOfStudentsModel();
$typeActivity=new activityOfStudentsModel();
$users=$userModel->users();
$result=$offerModel->offerList();
$offer_type=$typeActivity->studentsActivity();
//set number of page with parameter p from url
if(isset($_GET['p']))
{
    $pageNumber = $_GET['p'];
}
else
{
    $pageNumber = 1;
}
$limitPage = ((int)$pageNumber - 1) * $pageSize;
$limit_clause = " LIMIT ".$limitPage.",".$pageSize;

$offers=$offerModel->offerList(FALSE,$limit_clause);
//offers which are searched and without any filter
if(isset($_GET['search']) && (!isset($_GET['user']) || !isset($_GET['active']) || !isset($_GET['location'])))
{
    $result=$offerModel->searchOffer($_GET['search']);
    $offers=$offerModel->searchOffer($_GET['search'],$limit_clause);
    $pagination_url = 'index.php?view=offerlist&p=[p]&search='.$_GET['search'];
    $default_url = 'index.php?view=offerlist&search='.$_GET['search'];
}
//offers without search and without any filter
if(!isset($_GET['search']) && (!isset($_GET['user']) || !isset($_GET['active']) || !isset($_GET['location'])))
{
    $offers=$offerModel->offerList(FALSE,$limit_clause);
}
//offers without search and inside there are different combination of filters
if(!isset($_GET['search']))
{
    //offers if is set one of filter and inside is different combination of filters and search is not set
    if((isset($_GET['user']) || isset($_GET['active']) || isset($_GET['location'])))
    {
        if($_GET['user']!="")
        {
            $result=$offerModel->filterOffer($_GET['user']);
            $offers=$offerModel->filterOffer($_GET['user'],FALSE,FALSE,$limit_clause);

        }
        if($_GET['location']!="")
        {
            $result=$offerModel->filterOffer(FALSE,$_GET['location']);
            $offers=$offerModel->filterOffer(FALSE,$_GET['location'],FALSE,$limit_clause);

        }
        if($_GET['active']!="")
        {
            $result=$offerModel->filterOffer(FALSE,FALSE,$_GET['active']);
            $offers=$offerModel->filterOffer(FALSE,FALSE,$_GET['active'],$limit_clause);
        }
        if((isset($_GET['user']) && isset($_GET['active']) && isset($_GET['location'])))
        {
            $result=$offerModel->filterOffer($_GET['user'],$_GET['location'],$_GET['active'],FALSE);
            $offers=$offerModel->filterOffer($_GET['user'],$_GET['location'],$_GET['active'],$limit_clause);
        }
        $pagination_url = 'index.php?view=offerlist&p=[p]&user='.$_GET['user'].'&location='.$_GET['location'].'&active='.$_GET['active'];
        $default_url = 'index.php?view=offerlist&user='.$_GET['user'].'&location='.$_GET['location'].'&active='.$_GET['active'];
    }
}
//offers with search and inside there are different combination of filters
else
{
    //offers if is set one of filter and inside is different combination of filters and search is set
    if((isset($_GET['user']) || isset($_GET['active']) || isset($_GET['location'])))
    {
        if($_GET['user']!="")
        {
            $result=$offerModel->filterOffer($_GET['user'],FALSE,FALSE,FALSE,$_GET['search']);
            $offers=$offerModel->filterOffer($_GET['user'],FALSE,FALSE,$limit_clause,$_GET['search']);

        }
        if($_GET['location']!="")
        {
            $result=$offerModel->filterOffer(FALSE,$_GET['location'],FALSE,FALSE,$_GET['search']);
            $offers=$offerModel->filterOffer(FALSE,$_GET['location'],FALSE,$limit_clause,$_GET['search']);

        }
        if($_GET['active']!="")
        {
            $result=$offerModel->filterOffer(FALSE,FALSE,$_GET['active'],FALSE,$_GET['search']);
            $offers=$offerModel->filterOffer(FALSE,FALSE,$_GET['active'],$limit_clause,$_GET['search']);
        }
        if((isset($_GET['user']) && isset($_GET['active']) && isset($_GET['location'])))
        {
            $result=$offerModel->filterOffer($_GET['user'],$_GET['location'],$_GET['active'],FALSE,$_GET['search']);
            $offers=$offerModel->filterOffer($_GET['user'],$_GET['location'],$_GET['active'],$limit_clause,$_GET['search']);
        }
        $pagination_url = 'index.php?view=offerlist&p=[p]&user='.$_GET['user'].'&location='.$_GET['location'].'&active='.$_GET['active'].'&search='.$_GET['search'];
        $default_url = 'index.php?view=offerlist&user='.$_GET['user'].'&location='.$_GET['location'].'&active='.$_GET['active'].'&search='.$_GET['search'];
    }
}
$totalRecords = count($result);
$pg = new bootPagination();
$pg=HelperModel::setPagination($pg,$pageNumber,$pageSize,$totalRecords,$default_url,$pagination_url);
//if is submit form for update offer
if(isset($_POST['deadline_check_in']))
{
   $offerModel->updateOffer($_POST,$_POST['id']);
   header('location:index.php?view=offerlist');
}
//if is submit form for add offer
if(isset($_POST['add_deadline_check_in']))
{
    $offerModel->addOffer($_POST);
    header('location:index.php?view=offerlist');
}
?>
<div class="container">
    </br>
    <div class="row">
        <div class="col-3">
            Firme:
        </div>
        <div class="col-1">
        </div>
        <div class="col-3">
            Lokacija:
        </div>
        <div class="col-1">
        </div>

        <div class="col-3">
            Aktuelna/neaktuelna ponuda:
        </div>
        <div class="col-1">
        </div>
    </div>
    <div class="row">
        <div class="col-3">
            <select class="custom-select select-filter" name="filter_user" id="filter_user">
                <?php if(isset($_GET['user']) && $_GET['user']!=""): ?>
                    <option value="<?=$_GET['user'];?>"><?=$_GET['user'];?></option>
                    <option value="">Sve</option>
                <?php else: ?>
                    <option value="">Sve</option>
                <?php endif; $users_dist=$userModel->distinctUser();
                foreach($users_dist as $user):
                    if($user['naziv']!=$_GET['user'] && $user['naziv']!='Admin'):?>
                        <option value="<?= $user['naziv']; ?>"><?= $user['naziv']; ?></option>
                    <?php endif; endforeach; ?>
            </select>
        </div>
        <div class="col-1">
        </div>
        <div class="col-3">
            <select class="custom-select select-filter" name="filter_location" id="filter_location">
                <?php if(isset($_GET['location']) && $_GET['location']!=""): ?>
                    <option value="<?=$_GET['location'];?>"><?=$_GET['location'];?></option>
                    <option value="">Sve</option>
                <?php else: ?>
                    <option value="">Sve</option>
                <?php
                endif; $offers_select=$offerModel->distinctLocation();
                foreach($offers_select as $offer): ?>
                    <option value="<?= $offer['lokacija']; ?>"><?= $offer['lokacija']; ?></option>
                <?php  endforeach; ?>
            </select>
        </div>
        <div class="col-1">
        </div>
        <div class="col-3">
            <select class="custom-select select-filter" name="filter_active" id="filter_active">
                <?php if(isset($_GET['active']) && $_GET['active']!=""):
                    if($_GET['active']=="active"):?>
                        <option value="<?=$_GET['active'];?>">Aktuelne ponude</option>
                        <option value="">Sve</option>
                        <option value="non_active">Neaktuelne ponude</option>
                    <?php endif; if($_GET['active']=="non_active"):?>
                    <option value="<?=$_GET['active'];?>">Neaktuelne ponude</option>
                    <option value="">Sve</option>
                    <option value="non_active">Aktuelne ponude</option>
                <?php endif; else: ?>
                    <option value="">Sve</option>
                    <option value="active">Aktuelne ponude</option>
                    <option value="non_active">Neaktuelne ponude</option>
                <?php endif; ?>
            </select>
        </div>
        <div class="col-1">
            <button type="button" class="btn btn-primary float-right" id="clear_filter" disabled="true">Ponisti</button>
        </div>
    </div>
    </br>
    <div class="row">
        <div class="col-6">
            <?php if(isset($_SESSION['is_logged']) && $_SESSION['role']==1): ?>
            <button type="button" class="btn btn-primary float-left" data-toggle="modal" data-target="#add_offer">
                    Dodaj ponudu
            </button>
            <?php endif; ?>
        </div>

        <div class="col-2">
            <img class="float-right" style="width: 20px;height: 30px;" src="<?= FULL_URL_PATH;?>Assets/icons/search.png">
        </div>
        <div class="col-3">
            <?php if (isset($_GET['search'])):
                ?>
                <input class="float-right form-control" type="search" placeholder="Pretraga..." id="search_offers" value=<?= $_GET['search'];?>>
            <?php else:  ?>
                <input class="float-right form-control" type="search" placeholder="Pretraga..." id="search_offers">
            <?php endif; ?>
        </div>
        <div class="col-1">
            <button type="button" class="btn btn-primary float-right" id="clear_search_offers" disabled="true">Ponisti</button>
        </div>
    </div>
    </br>
    <div id="result_offers">
        <?php if(!empty($offers)):
            include_once FULL_FILE_PATH.'Views/tables/table_offerlist.php';
        else:
         ?>
            <div class="alert alert-info" role="alert">Nema rezultata</div>
        <?php
          endif;
      ?>
    </div>
</div>
<?php

include FULL_FILE_PATH."Views/modals/addoffer.php";
include FULL_FILE_PATH."Views/modals/editoffer.php";
?>
