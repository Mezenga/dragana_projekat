<head>
		<title>Konkursi za studente</title>
		<meta charset="UTF-8">
		<meta name="keywords" content="HTML,CSS,XML,JavaScript">
		<meta name="author" content="Dragana Cajic">
		<meta name="description" content="Konkursi za prakse, stipendije, radionice, zavrsne radove itd.">
		<meta name="viewport" content="width=device-width">
        <script src="<?= FULL_URL_PATH;?>Assets\jquery-ui-1.12.1.custom\external\jquery\jquery.js"></script>
		<script src="<?= FULL_URL_PATH;?>Assets\jquery-ui-1.12.1.custom\jquery-ui.js"></script>
        <link rel="stylesheet" href="<?= FULL_URL_PATH;?>Assets\jquery-ui-1.12.1.custom\jquery-ui.css">
        <link rel="stylesheet" href="<?= FULL_URL_PATH;?>Assets\jquery-ui-1.12.1.custom\jquery-ui.structure.css">
        <link rel="stylesheet" href="<?= FULL_URL_PATH;?>Assets\jquery-ui-1.12.1.custom\jquery-ui.theme.css">
		<link rel="stylesheet" href="<?= FULL_URL_PATH;?>Assets/bootstrap-4.0.0-dist/css/bootstrap.min.css">
		<script src="<?= FULL_URL_PATH;?>Assets/bootstrap-4.0.0-dist/js/bootstrap.min.js"></script>
        <script src="<?= FULL_URL_PATH;?>Assets/bootstrap-4.0.0-dist/js/bootstrap.bundle.js"></script>
		<script src="<?= FULL_URL_PATH;?>Assets/script.js"></script>
</head>