<?php
/**
 * Created by Dragana Cajic.
 * User: Dragana
 */
if(isset($_GET['logout']))
  {
      $usersModel->logout();
      header('Location:index.php?logout=true');
  }
?>
<nav class="navbar navbar-expand-lg bg-primary navbar-dark">
  <ul class="navbar-nav mr-auto">
      <?php if(!isset($_GET['view'])):
           // header('location:index.php?view=userslist&reg=true');
      endif;
      if($_SESSION['role']==2):
           if($_GET['view']=='userslist' && isset($_GET['reg'])): ?>
           <li class="nav-item active" id="users">
           <?php else: ?>
               <li class="nav-item" id="users">
           <?php endif; ?>
      <a class="nav-link" href="<?=FULL_URL_PATH;?>index.php?view=userslist&reg=true">Lista aktivnih korisnika</a>
    </li>
         <?php  if($_GET['view']=='activitylist'): ?>
    <li class="nav-item active">
        <?php else: ?>
             <li class="nav-item">
                 <?php endif; if($_GET['view']=='userslist' && !isset($_GET['reg'])): ?>
           <li class="nav-item active" id="users">
       <?php else: ?>
           <li class="nav-item" id="users">
       <?php endif; ?>
           <a class="nav-link" href="<?=FULL_URL_PATH;?>index.php?view=userslist">Lista korisnika na cekanju</a>
           </li>
           <?php endif; if($_GET['view']=='activitylist'): ?>
           <li class="nav-item active">
       <?php else: ?>
           <li class="nav-item">
       <?php endif; ?>
        <a class="nav-link" href="<?=FULL_URL_PATH;?>index.php?view=activitylist">Djelatnosti</a>
    </li>
       <?php

    if($_GET['view']=='studentsActivityList'):?>
      <li class="nav-item active">
          <?php else: ?>
      <li class="nav-item">
          <?php endif; ?>
          <a class="nav-link" href="<?=FULL_URL_PATH;?>index.php?view=studentsActivityList">Aktivnosti</a>
      </li>
      <?php
      if($_GET['view']=='offerlist' && !isset($_GET['user'])):?>
      <li class="nav-item active">
          <?php else: ?>
      <li class="nav-item">
          <?php endif; ?>
          <a class="nav-link" href="<?=FULL_URL_PATH;?>index.php?view=offerlist">Ponude</a>
      </li>
      <?php if($_SESSION['role']==1){
      if($_GET['view']=='offerlist' && isset($_GET['user']) && $_GET['user']==$_SESSION['name']):?>
      <li class="nav-item active">
          <?php else: ?>
      <li class="nav-item">
          <?php endif; ?>
          <a class="nav-link" href=<?=FULL_URL_PATH;?>index.php?view=offerlist&location=&active=&user=<?=$_SESSION['name']; ?>>Moje ponude</a>
      </li>
      <?php } ?>
  </ul>
  <ul class="navbar-nav ml-auto">
    <li class="nav-item active dropdown">
        <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-left:100px">
          <?php echo $_SESSION["name"]; ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" id="change" href="?view=editdata&id=<?=$_SESSION['id'];?>">Uredi profil</a>
          <a class="dropdown-item" href='<?=FULL_URL_PATH;?>index.php?logout=true'>Odjavi se</a>
        </div>
     </li>
  </ul>
</nav>


