<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 9/7/18
 * Time: 10:09 AM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
$activityModel=new activityModel();
$activity=$activityModel->typeActivity();
$exist=0;
if(HelperModel::isAjax()==true)
{
    //input field for name of user activity must be fill
    if($_POST['pop_name_user_activity']!="")
    {
        foreach($activity as $user_activity)
        {
            //check if name of user activity already exist in database
            if($_POST['pop_name_user_activity']==$user_activity['naziv_vrste_djelatnosti'])
            {
                $exist++;
                print json_encode(array('status' => false,'content'=>'Djelatnost vec postoji u bazi!'));
                die();
            }
        }
        //if both condition are ok insert that activity in database
        if($exist==0)
        {
            $activityModel->addActivity($_POST['pop_name_user_activity'],$_POST['pop_description_user_activity']);
            $user_activity=$activityModel->typeActivity();
            $data=" <select class='custom-select' name='activity'>";
            foreach($user_activity as $activity)
            {
                if($activity['naziv_vrste_djelatnosti']==$_POST['pop_name_user_activity'])
                {
                    $data.="<option value=".$activity['idVrste'].">".$activity['naziv_vrste_djelatnosti']."</option>";
                }
            }
            foreach($user_activity as $activity)
            {
                if($activity['naziv_vrste_djelatnosti']!=$_POST['pop_name_user_activity'])
                {
                    $data.="<option value=".$activity['idVrste'].">".$activity['naziv_vrste_djelatnosti']."</option>";
                }
            }
            $data.="</select>";
            print json_encode(array('status' => true,'content'=>$data));
        }
    }
    else
    {
        print json_encode(array('status' => false,'content'=>'Unesite naziv!'));
    }
}