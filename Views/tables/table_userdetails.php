<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/24/18
 * Time: 9:19 AM
 */
?>
<div class="container">
    <table class="table table-bordered table-hover">
        <thead class="text-center table-active">
        <tr class="table-active">
            <th>Redni broj</th>
            <th>Naziv</th>
            <th>Email</th>
            <th>Kontakt adresa</th>
            <th>Web sajt</th>
            <?php if($result['idUloge']==1): ?>
                <th>Vrsta djelatnosti</th>
                <th>Opis djelatnosti</th>
            <?php  endif;?>
            <th>Pristup</th>

        </tr>
        </thead>
        <tbody>
        <?php
            $broj=1;
        ?>
        <tr>
            <td class='text-center table-active'><?= $broj;?>.</td>
            <td><?= $result['naziv'];?></td>
            <td><?= $result['email'];?></td>
            <td><?= $result['kontakt_adresa'];?></td>
            <td><a href=http://<?= $result['web_sajt'];?> target=_blank><?= $result['web_sajt'];?></a></td>
            <?php if($result['idUloge']==1): ?>
            <td><?= $result['naziv_vrste_djelatnosti'];?></td>
            <td><?= $result['opis_djelatnosti']; endif;?></td>
            <td>
                <?php   if($result['odobren_pristup']=='0'):?>
                    Nije odobren!
                <?php else: ?>
                    Odobren
                <?php endif;?>
            </td>
        </tr>
        <?php
        $broj++;
        ?>
        </tbody>
    </table>
</div>