<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/20/18
 * Time: 4:36 PM
 */
?>
<div class="row">
<div class="col">
    <table class="table table-bordered table-hover">
        <thead class="text-center table-primary">
        <tr class="table-active">
            <th>Redni broj</th>
            <th>Naziv</th>
            <th>Opis</th>
            <?php if($_SESSION['role']==2): ?>
            <th>Opcije</th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody>
        <?php
        if(isset($_GET['p']))
        {
            $number=($_GET['p']-1)*10+1;
        }
        else
        {
            $number=1;
        }
        foreach ($activity as $type_activity):
            ?>
            <tr>
                <td class='text-center'><?= $number;?>.</td>
                <?php if(isset($_SESSION['is_logged']) && $_SESSION['role']==2): ?>
                <td><a href=<?=FULL_URL_PATH;?>index.php?view=usersOfActivity&id=<?=$type_activity['idVrste'];?>><?= $type_activity['naziv_vrste_djelatnosti'];?></a></td>
                <?php else: ?>
                <td><?= $type_activity['naziv_vrste_djelatnosti'];?></td>
                <?php endif; ?>
                <td><?= $type_activity['opis_djelatnosti'];?></td>
                <?php if($_SESSION['role']==2): ?>
                <td class='text-center'>
                    <a href=<?=FULL_URL_PATH;?>index.php?view=editactivity&id=<?=$type_activity['idVrste'];?>><img title="Izmijeni" style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/edit-512.png"></a>
                    <a href="<?=FULL_URL_PATH;?>index.php?view=usersOfActivity&id=<?=$type_activity['idVrste'];?>"><img  title="Korisnici sa ovom djelatnoscu" style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/View.png"></a>
                    <a href="#"><img  title="Obrisi" onclick="DeleteActivity(<?=$type_activity['idVrste'];?>);"style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/free-27-512.png"></a>
                </td>
                <?php endif; ?>
            </tr>
            <?php
            $number++;
        endforeach;
        ?>
        </tbody>
    </table>
</div>
</div>
<div class="row">
    <div class="col-9">
        <?php
        echo $pg->process();
        ?>
    </div>
    <div class="col-3 float-right">
        <span class="float-right text-primary"> Ukupno:&nbsp;<?=$pg->totalrecords ?></span>
    </div>
</div>