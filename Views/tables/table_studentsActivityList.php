<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/20/18
 * Time: 4:38 PM
 */
?>
<div class="row">
<div class="col">
    <table class="table table-bordered table-hover">
        <thead class="text-center table-primary">
        <tr class="table-active">
            <th>Redni broj</th>
            <th>Naziv</th>
            <th>Opis</th>
            <?php if($_SESSION['role']==2):?>
                <th>Opcije</th>
            <?php endif; ?>

        </tr>
        </thead>
        <tbody>
        <?php
        if(isset($_GET['p']))
        {
            $number=($_GET['p']-1)*10+1;
        }
        else
        {
            $number=1;
        }
        foreach ($activity as $type_activity):
            ?>
            <tr>
                <td class='text-center'><?= $number;?>.</td>
                <?php if(isset($_SESSION['is_logged']) && $_SESSION['role']==2): ?>
                <td><a href=<?=FULL_URL_PATH;?>index.php?view=offersOfStudentActivity&id=<?=$type_activity['idTipa'];?>><?= $type_activity['naziv_tipa_aktivnosti'];?></a></td>
                <?php else: ?>
                <td><?= $type_activity['naziv_tipa_aktivnosti'];?></td>
                <?php endif; ?>
                <td><?= $type_activity['opis_aktivnosti'];?></td>
            <?php if($_SESSION['role']==2):?>
                <td class='text-center'>
                    <a href='#'><img onclick="EditStudentActivity(<?= $type_activity['idTipa'];?>);" title="Izmijeni" style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/edit-512.png"></a>
                    <a href="<?=FULL_URL_PATH;?>index.php?view=offersOfStudentActivity&id=<?=$type_activity['idTipa'];?>"><img  title="Ponude za ovu aktivnost" style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/View.png"></a>
                    <a href="#"><img  title="Obrisi" onclick="DeleteStudentActivity(<?=$type_activity['idTipa'];?>);"style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/free-27-512.png"></a>
                </td>
            <?php endif; ?>
            </tr>
            <?php
            $number++;
        endforeach;
        ?>
        </tbody>
    </table>
</div>
    </div>
<div class="row">
    <div class="col-9">
        <?=$pg->process(); ?>
    </div>
    <div class="col-3">
        <span class="float-right text-primary"> Ukupno:&nbsp;<?=$pg->totalrecords ?></span>
    </div>
</div>