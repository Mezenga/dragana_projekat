<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/20/18
 * Time: 3:24 PM
 */ ?>

<div class="row">
<div class="col">
    <table class="table table-bordered table-hover container">
        <thead class="text-center table-primary">
        <tr class="table-active">
            <th>Redni broj</th>
            <th>Naziv</th>
            <th>Email</th>
            <th>Kontakt adresa</th>
            <th>Web sajt</th>
            <?php if($_SESSION['role']==2):?>
            <th>Opcije</th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody>
        <?php
        if(isset($_GET['p']))
        {
            $number=($_GET['p']-1)*10+1;
        }
        else
        {
            $number=1;
        }
        foreach ($result_us as $user):
            ?>
            <tr>
                <td class='text-center'><?= $number;?>.</td>
                <?php if(isset($_SESSION['is_logged']) && $_SESSION['role']==2): ?>
                <td> <a href="<?=FULL_URL_PATH; ?>index.php?view=details&id=<?= $user['idKorisnika']; ?>"><?= $user['naziv'];?></a></td>
                <?php else: ?>
                <td><?= $user['naziv'];?></td>
                <?php endif; ?>
                <td><?= $user['email'];?></td>
                <td><?= $user['kontakt_adresa'];?></td>
                <td><a href=http://<?= $user['web_sajt'];?> target=_blank><?= $user['web_sajt'];?></a></td>
                <?php if($_SESSION['role']=='2'): ?>
                    <td class='text-center'>
                        <a href=<?=FULL_URL_PATH;?>index.php?view=editdata&reg=<?php if(isset($_GET['reg'])): echo $_GET['reg']; endif; ?>&id=<?=$user['idKorisnika'];?>><img title="Izmijeni" style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/edit-512.png"></a>
                        <a href=<?=FULL_URL_PATH;?>index.php?view=details&id=<?=$user['idKorisnika'];?>><img  title="Detalji" style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/View.png"></a>
                        <a href="#"><img  title="Obrisi" onclick="Delete(<?=$user['idKorisnika'];?>);" style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/free-27-512.png"></a>
                        <?php if(!isset($_GET['reg'])): ?>
                        <a href="<?=FULL_URL_PATH;?>index.php?view=allowaccess&access=true&id=<?=$user['idKorisnika'];?>"><img  title="Odobri" style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/check-mark-aco-group.png"></a>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
            </tr>
            <?php
            $number++;
        endforeach;
        ?>
        </tbody>
    </table>
</div>
</div>

<div class="row">
    <div class="col-9">
        <?php
        echo $pg->process();
        ?>
    </div>
    <div class="col-3 float-right">
        <span class="float-right text-primary"> Ukupno:&nbsp;<?=$pg->totalrecords ?></span>
    </div>
</div>
