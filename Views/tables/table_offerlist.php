<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/22/18
 * Time: 8:43 AM
 */
 ?>
<div class="row">
    <div class="col">
        <table class="table table-bordered table-hover">
            <thead class="text-center table-primary">
            <tr class="table-active">
                <th>Redni broj</th>
                <th>Aktivnost</th>
                <th>Poslodavac</th>
                <th>Ciklus studija</th>
                <th>Rok za prijavu</th>
                <?php if(isset($_SESSION['role']) && $_SESSION['role']==2):?>
                    <th>Opcije</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php
            if(isset($_GET['p']))
            {
                $number=($_GET['p']-1)*10+1;
            }
            else
            {
                $number=1;
            }
            foreach ($offers as $offer):

                ?>
                    <tr>

                    <td class='text-center'><?= $number;?>.</td>
                    <td><a href=<?=FULL_URL_PATH;?>index.php?view=detailsoffer&id=<?=$offer['idPonude'];?>><?= $offer['naziv_tipa_aktivnosti'];?></a></td>
                    <td><a href=<?=FULL_URL_PATH;?>index.php?view=details&id=<?=$offer['idKorisnika'];?>><?= $offer['naziv'];?></a></td>
                    <td><?= $offer['ciklus_studija'];?></td>
                    <td><?= $offer['rok_za_prijavu'];?></td>
                    <?php if(isset($_SESSION['role']) && $_SESSION['role']=='2'): ?>
                        <td class='text-center'>
                            <a href="#"><img title="Izmijeni" onclick="EditOffer(<?=$offer['idPonude'];?>);" style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/edit-512.png"></a>
                            <a href=<?=FULL_URL_PATH;?>index.php?view=detailsoffer&id=<?=$offer['idPonude'];?>><img  title="Detalji" style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/View.png"></a>
                            <a href="#"><img  title="Obrisi" onclick="DeleteOffer(<?=$offer['idPonude'];?>);" style="width:25px;height:25px;" src="<?= FULL_URL_PATH;?>Assets/icons/free-27-512.png"></a>
                        </td>
                    <?php endif; ?>
                </tr>
                <?php
                $number++;
            endforeach;
            ?>
            </tbody>
        </table>
    </div>
</div>
<?php if(!isset($_POST['id'])): ?>
    <div class="row">
        <div class="col-9">
            <?php
            echo $pg->process();
            ?>
        </div>
        <div class="col-3 float-right">
            <span class="float-right text-primary">Ukupno:&nbsp;<?=$pg->totalrecords ?></span>
        </div>
    </div>
<?php endif;
    ?>