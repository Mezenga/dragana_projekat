<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/22/18
 * Time: 9:48 AM
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
$offerModel=new offerModel();
$typeActivity=new activityOfStudentsModel();
$offer_type=$typeActivity->studentsActivity();
if(isset($_POST['id_edit']))
{
$offers=$offerModel->offerList($_POST['id_edit']);
?>
    <div class="alert alert-warning" role="alert" id="message_edit_offer" hidden="true"></div>

    <div class="form-group">
    <input type="text" hidden="true" value="<?= $_POST['id_edit'];?>" name="id">
</div>
<div class="form-group">
    <label for="name_type">Tip ponude:</label>
    <select class="custom-select" name="offer_type">
            <option value="<?= $offers['idTipa']; ?>"><?= $offers['naziv_tipa_aktivnosti']; ?></option>
        <?php foreach($offer_type as $type):
            if($type['idTipa']!=$offers['idTipa']): ?>
                <option value="<?= $type['idTipa']; ?>"><?= $type['naziv_tipa_aktivnosti']; ?></option>
         <?php endif;   endforeach; ?>
    </select>
</div>
<div class="form-group">
    <label for="study">Ciklus studija:</label>
    <input type="text" class="form-control" value="<?= $offers['ciklus_studija']; ?>" name="study" id="study">
</div>
<div class="form-group">
    <label for="deadline_check_in">Rok za prijavu:</label>
    <input type="text" class="form-control" value="<?= $offers['rok_za_prijavu']; ?>" name="deadline_check_in" id="deadline_check_in">
</div>
<div class="form-group">
    <label for="max_number">Maksimalan broj kandidata:</label>
    <input type="text" class="form-control" value="<?= $offers['maksimalan_broj_kandidata']; ?>" name="max_number" id="max_number">
</div>
<div class="form-group">
    <label for="location">Lokacija:</label>
    <input type="text" class="form-control" value="<?= $offers['lokacija']; ?>" name="location" id="location">
</div>
<div class="form-group">
    <label for="offer_description">Opis ponude:</label>
    <input type="text" class="form-control" name="description_offer" id="description_offer" value="<?= $offers['opis_ponude']; ?>">
</div>
<div class="form-group">
    <label for="start">Pocetak:</label>
    <input type="text" class="form-control" value="<?= $offers['pocetak']; ?>" name="start" id="start">
</div>
<div class="form-group">
    <label for="duration">Trajanje:</label>
    <input type="text" class="form-control" value="<?= $offers['trajanje']; ?>" name="duration" id="duration">
</div>
<?php } ?>