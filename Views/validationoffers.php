<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/22/18
 * Time: 3:17 PM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
if(HelperModel::isAjax()==true)
{
    //this parameter is set if we want to validate input field when we insert or update new offer
    if(isset($_REQUEST['offers_validation']))
    {
        header('Content-Type: application/json');
        //this parameter is add when we validate insert new offer
        if($_REQUEST['offers_validation']=='add')
        {
            //all field have to be fill
            if($_POST['add_study']!="" && $_POST['add_deadline_check_in']!="" && $_POST['add_max_number']!="" && $_POST['add_location']!="" && $_POST['add_duration']!="" && $_POST['add_start']!="" && $_POST['add_description_offer']!="")
            {
                //number of candidates mas be number
                if(is_numeric($_POST['add_max_number']))
                {
                    //deadline must be date
                    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_POST['add_deadline_check_in']))
                    {
                        //duration must be date
                        //only if all condition are ok return true
                        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_POST['add_start']))
                        {
                            print json_encode(array('status' => true));
                        }
                        else
                        {
                            print json_encode(array('status' => false, 'message' => 'Nije dobar format datuma za pocetak!'));
                        }
                    } else {
                        print json_encode(array('status' => false, 'message' => 'Nije dobar format datuma za rok prijave!'));
                    }
                }
                else
                {

                    print json_encode(array('status' => false, 'message' => 'Broj kandidata mora biti cijeli broj!'));
                }
            }
            else
            {
                print json_encode(array('status' => false, 'message' => 'Popunite sva polja'));
            }

        }
        //this parameter is edit when we validate new information about some offer
        if($_REQUEST['offers_validation']=='edit')
        {
            //all field have to be fill
            if($_POST['study']!="" && $_POST['deadline_check_in']!="" && $_POST['max_number']!="" && $_POST['location']!="" && $_POST['duration']!="" && $_POST['start']!="" && $_POST['description_offer']!="")
            {
                //number of candidates mas be number
                if(is_numeric($_POST['max_number']))
                {
                    //deadline must be date
                    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_POST['deadline_check_in']))
                    {
                        //duration must be date
                        //only if all condition are ok return true
                        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_POST['start']))
                        {
                            print json_encode(array('status' => true));
                        }
                        else
                        {
                            print json_encode(array('status' => false, 'message' => 'Nije dobar format datuma za pocetak!'));
                        }
                    } else {
                        print json_encode(array('status' => false, 'message' => 'Nije dobar format datuma za rok prijave!'));
                    }
                }
                else
                {

                    print json_encode(array('status' => false, 'message' => 'Broj kandidata mora biti cijeli broj!'));
                }
            }
            else
            {
                print json_encode(array('status' => false, 'message' => 'Popunite sva polja'));

            }
        }

    }
}