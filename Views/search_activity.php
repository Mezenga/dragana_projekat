<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/21/18
 * Time: 3:12 PM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'\dragana_projekat\config\loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
//this file use only for ajax  request when we searching users activity
if(HelperModel::isAjax()==true)
{
    $activityModel=new activityModel();

    $result=$activityModel->searchActivity($_GET['search']);
    $and_clause = '';
    $default_url = 'index.php?view=activitylist';
    $pagination_url = 'index.php?view=activitylist&p=[p]&search='.$_GET['search'];
    $search_term = '';
    $pageSize = 10;
    //set number of page on 1 or on parameters p is ti is set
    if(isset($_GET['p']))
    {
        $pageNumber = $_GET['p'];
    }
    else
    {
        $pageNumber = 1;
    }
    $limitPage = ((int)$pageNumber - 1) * $pageSize;
    $limit_clause = " LIMIT ".$limitPage.",".$pageSize;
    $totalRecords = count($result);
    $pg = new bootPagination();
    $pg=HelperModel::setPagination($pg,$pageNumber,$pageSize,$totalRecords,$default_url,$pagination_url);
    $activity=$activityModel->searchActivity($_GET['search'],$limit_clause);
    if(!empty($activity)):
        ?>
        <div class="text-center" id="loading_activity" hidden="true">
            <img src="<?= FULL_URL_PATH;?>Assets/icons/blue-loading-gif-transparent-9.gif">
        </div>
        <?php include_once FULL_FILE_PATH.'Views\tables\table_activitylist.php'; ?>
    <?php
    else:
        ?>
        <div class="alert alert-warning" role="alert">Nema rezultata!</div>
    <?php
    endif;
}
?>




