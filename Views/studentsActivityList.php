<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/16/18
 * Time: 9:24 AM
 */
$activityStudentsModel=new activityOfStudentsModel();
$and_clause = '';
$default_url = 'index.php?view=studentsActivityList';
$pagination_url = 'index.php?view=studentsActivityList&p=[p]';
$search_term = '';
$pageSize = 10;
$result=$activityStudentsModel->studentsActivity();
//insert student activity if form from modal is submit and validate
if(isset($_POST['add_name_student_activity']))
{
    $activityStudentsModel->insertStudentActivity($_POST['add_name_student_activity'],$_POST['add_description_student_activity']);
}
//update student activity if form from modal is submit and validate
if(isset($_POST['n_student_activity']))
{
    $activityStudentsModel->updateStudentActivity($_POST);
}
//set number of page on 1 or on parameter p if it is set
if(isset($_GET['p']))
{
    $pageNumber = $_GET['p'];
}
else
{
    $pageNumber = 1;
}
$limitPage = ((int)$pageNumber - 1) * $pageSize;
$limit_clause = " LIMIT ".$limitPage.",".$pageSize;
$activity=$activityStudentsModel->studentsActivity(FALSE,$limit_clause);
//result of students activity if input filed search is not empty
if(isset($_GET['search']))
{
    $result=$activityStudentsModel->searchStudentActivity($_GET['search']);
    $activity=$activityStudentsModel->searchStudentActivity($_GET['search'],$limit_clause);
    $pagination_url = 'index.php?view=studentsActivityList&p=[p]&search='.$_GET['search'];
    $default_url = 'index.php?view=studentsActivityList&search='.$_GET['search'];
}
//result of students activity if input filed search is empty
else
{
    $result=$activityStudentsModel->searchStudentActivity(FALSE,FALSE);
    $activity=$activityStudentsModel->studentsActivity(FALSE,$limit_clause);
}
$totalRecords = count($result);
$pg = new bootPagination();
$pg=HelperModel::setPagination($pg,$pageNumber,$pageSize,$totalRecords,$default_url,$pagination_url);
?>
<div class="container">
   </br>
   <div class="row">
        <div class="col-6">
            <?php
            if($_SESSION['role']=='2'):?>
            <button type="button" class="btn btn-primary float-left" data-toggle="modal" data-target="#AddStudentsActivity">
                Dodaj aktivnost
            </button>
           <?php endif;
            ?>
         </div>
        <div class="col-2">
        <img class="float-right" style="width: 20px;height: 30px;" src="<?= FULL_URL_PATH;?>Assets/icons/search.png">
        </div>
        <div class="col-3">
        <?php if (isset($_GET['search'])):?>
            <input class="float-right form-control" type="search" placeholder="Pretraga..." id="search_studentactivity" value=<?= $_GET['search'];?>>
        <?php else:  ?>
            <input class="float-right form-control" type="search" placeholder="Pretraga..." id="search_studentactivity">
        <?php endif; ?>
        </div>
       <div class="col-1">
           <button type="button" class="btn btn-primary float-right" id="clear_search_student_activity" disabled="true">Ponisti</button>
       </div>
    </div>
    </br>
    <div id="result_studentactivity">
        <?php
        include FULL_FILE_PATH."/Views/tables/table_studentsActivityList.php";
        ?>
    </div>
</div>
<?php
include FULL_FILE_PATH."Views/modals/editstudentactivity.php";
include FULL_FILE_PATH."/Views/modals/addStudentActivity.php";
?>