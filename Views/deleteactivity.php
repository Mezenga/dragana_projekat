<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/10/18
 * Time: 2:14 PM
 */
require_once FULL_FILE_PATH.'config/loader_models.php';
$activityModel=new activityModel();
$userModel=new usersModel();
$exist_ac=1;
if(isset($_GET['id']))
{
    $result=$activityModel->typeActivity($_GET['id']);
    $users=$userModel->users(FALSE,FALSE,TRUE);
    $users+=$userModel->users();
    foreach($users as $user)
    {
        if($user['idVrste']==$_GET['id'])
        {
            $exist_ac=0;
        }
    }
    if($exist_ac==1)
    {
        if(!empty($result))
        {
            $activityModel->deleteActivity($_GET['id']);
            header('Location:index.php?view=activitylist');
        }
        else
        {
           header('Location:index.php?view=withoutview');
        }
    }
    else
    {
        echo '<div class="alert alert-warning" role="alert">Nije moguće obrisati djelatnost koja je dodjeljena nekom korisniku!</div>';
    }
}
