<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/16/18
 * Time: 9:32 AM
 */
?>

<div class="modal fade" id="AddStudentsActivity">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Aktivnost</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="#" name="submit_activity" id="add_form_student_activity" method="post" autocomplete="off">
                <div class="alert alert-warning" role="alert" id="add_message" hidden="true"></div>
                <div class="modal-body">
                    <div class="alert alert-warning" role="alert" id="message" hidden="true"></div>
                    <div class="form-group">
                        <label for="name">Naziv aktivnosti:</label>
                        <input type="text" title="Obavezan unos!"  class="form-control" name="add_name_student_activity" placeholder="Unesite naziv aktivnosti">
                    </div>
                    <div class="form-group">
                        <label for="description">Opis aktivnosti:</label>
                        <input type="text" title="Obavezan unos!" class="form-control"  name="add_description_student_activity" placeholder="Unesite opis aktivnosti">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="ValidationAddStudentActivity();" id="submit_student_activity" name="submit_student_activity">Sacuvaj</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
                </div>
            </form>
        </div>
    </div>
</div>
