<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/23/18
 * Time: 9:09 AM
 */

?>
<div class="modal" id="mod_registration">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registracija</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
                  <form action="" method="post" id="registration_form" autocomplete="off" autocomplete="off">
                      <div class="alert alert-warning" role="alert" id="message_reg" hidden="true"></div>
                        <div class="modal-body">

                        <div class="form-group">
                            <label for="name">Naziv:</label>
                            <input type="text" class="border form-control" id="reg_name" name="name" placeholder="Unesite naziv firme">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="border form-control" id="reg_email" name="email" placeholder="Unesite email">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Lozinka:</label>
                            <input type="password" class="border form-control" id="reg_pwd" name="password" placeholder="Unesite lozinku">
                        </div>
                        <div class="form-group">
                            <label for="address">Kontakt adresa:</label>
                            <input type="text" class="form-control" id="reg_address" name="address" placeholder="Unesite kontakt adresu">
                        </div>
                        <div class="form-group">
                            <label for="web_site">Web sajt:</label>
                            <input type="text" class="form-control" id="reg_web_site" name="web_site" placeholder="Unesite web sajt">
                        </div>
                        <div class="form-group">
                            <label for="name_type">Vrsta djelatnosti:</label>
                            <select class="custom-select" name="activity">
                                <?php foreach($type_activity as $activity): ?>
                                    <option value="<?= $activity['idVrste']; ?>" title="<?= $activity['opis_djelatnosti']; ?>"><?= $activity['naziv_vrste_djelatnosti']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">Opis djelatnosti:</label>
                            <input type="text" class="form-control" id="desc" name="desc" placeholder="Unesite opis vase djelatnosti">
                        </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="registration" name="registration">Potvrdi</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Odustani</button>
            </div>
                  </form>
        </div>
    </div>
</div>
