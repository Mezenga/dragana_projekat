<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/10/18
 * Time: 2:00 PM
 */
?>
<div class="modal" id="add_activity_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Djelatnost</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="#" name="submit_activity"  method="post" onsubmit="return checkBlankAddActivity();" autocomplete="off">
                <div class="alert alert-warning" role="alert" id="message_activity" hidden="true"></div>
                <div class="modal-body">

                <div class="form-group">
                    <label for="name">Naziv djelatnosti:</label>
                    <input type="text" title="Obavezan unos!" class="border form-control" id="name_activity" name="name_type" placeholder="Unesite naziv djelatnosti">
                </div>
                <div class="form-group">
                    <label for="description">Opis djelatnosti:</label>
                    <input type="text" class="form-control" name="description_type" id="description_type" placeholder="Unesite opis djelatnosti">
                </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name="submit_activity">Sacuvaj</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
                </div>
            </form>
        </div>
    </div>
</div>