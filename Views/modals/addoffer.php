<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/22/18
 * Time: 1:36 PM
 */
?>
<div class="modal fade" id="add_offer">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Nova ponuda</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="#" name="submit_activity" id="form_add_offer" method="post" autocomplete="off">
                <div class="modal-body">
                    <div class="alert alert-warning" role="alert" id="message_offer" hidden="true"></div>
                    <div class="form-group">
                        <label for="name_type">Aktivnost:</label>
                        <div class="row">
                            <div class="col-10" id="select_st_activity">
                                <select class="custom-select" name="add_offer_type">
                                    <?php foreach($offer_type as $type): ?>
                                            <option value="<?= $type['idTipa']; ?>"><?= $type['naziv_tipa_aktivnosti']; ?></option>
                                        <?php endforeach; ?>
                                </select>
                            </div>

                                <button type="button" class="btn btn-primary col-1.5" data-toggle="popover" id="popover_student_activity" data-container="body">Dodaj</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="study">Ciklus studija:</label>
                        <input type="text" class="form-control" name="add_study" id="study">
                    </div>
                    <div class="form-group">
                        <label for="deadline_check_in">Rok za prijavu:</label>
                        <input type="text" class="form-control" name="add_deadline_check_in" id="deadline_check_in">
                    </div>
                    <div class="form-group">
                        <label for="max_number">Maksimalan broj kandidata:</label>
                        <input type="text" class="form-control" name="add_max_number" id="max_number">
                    </div>
                    <div class="form-group">
                        <label for="location">Lokacija:</label>
                        <input type="text" class="form-control" name="add_location" id="location">
                    </div>
                    <div class="form-group">
                        <label for="offer_description">Opis ponude:</label>
                        <input type="text" class="form-control" name="add_description_offer" id="add_description_offer">
                    </div>
                    <div class="form-group">
                        <label for="start">Pocetak:</label>
                        <input type="text" class="form-control" name="add_start" id="start">
                    </div>
                    <div class="form-group">
                        <label for="duration">Trajanje:</label>
                        <input type="text" class="form-control" name="add_duration" id="duration">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="add_sub_offer" name="add_sub_offer">Sacuvaj</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="popover_student_activity_content" hidden="true">
    <form action="#" id="popover_add_form_student_activity" method="post">
        <div class="alert alert-warning" role="alert" id="pop_add_message" hidden="true"></div>
        <div class="form-group">
            <label for="name">Naziv aktivnosti:</label>
            <input type="text" title="Obavezan unos!"  class="border form-control" name="pop_add_name_student_activity" placeholder="Unesite naziv aktivnosti">
        </div>
        <div class="form-group">
            <label for="description">Opis aktivnosti:</label>
            <input type="text" title="Obavezan unos!"  class="form-control"  name="pop_add_description_student_activity" placeholder="Unesite opis aktivnosti">
        </div>
        <button type="button" class="btn btn-primary" id="pop_submit_student_activity" name="submit_student_activity">Sacuvaj</button>
        <button type="button" class="btn btn-secondary" id="pop_close_student_activity">Odustani</button>
    </form>
</div>