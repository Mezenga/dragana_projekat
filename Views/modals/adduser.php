<div class="modal fade" id="addUserModal">
    <div class="modal-dialog  modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Dodavanje novog korisnika</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="#" method="post" onsubmit="return checkBlank();" autocomplete="off">
                <div class="alert alert-warning" role="alert" id="message_user" hidden="true"></div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name">Naziv:</label>
                    <input type="text" class="border form-control" id="name_user" name="name" placeholder="Unesite naziv" title="Obavezan unos!">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" class="border form-control" id="email_user" name="email" placeholder="Unesite email" title="Obavezan unos!">
                </div>
                <div class="form-group">
                    <label for="pwd">Lozinka:</label>
                    <input type="password" class="border form-control" id="pwd_user" name="password" placeholder="Unesite lozinku" title="Obavezan unos!">
                </div>
                <div class="form-group">
                    <label for="address">Kontakt adresa:</label>
                    <input type="text" class="form-control" id="address_user" name="address" placeholder="Unesite kontakt adresu">
                </div>
                <div class="form-group">
                    <label for="web_site">Web sajt:</label>
                    <input type="text" class="form-control" id="web_site_user" name="web_site" placeholder="Unesite web sajt">
                </div>
                <div class="form-group">
                    <label for="name_type">Vrsta djelatnosti:</label>
                    <div class="row">
                        <div class="col-10" id="select_user_activity">
                            <select class="custom-select" name="activity">
                            <?php foreach($type_activity as $activity): ?>
                                <option value="<?= $activity['idVrste']; ?>" title="<?= $activity['opis_djelatnosti']; ?>"><?= $activity['naziv_vrste_djelatnosti']; ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                        <button type="button" class="btn btn-primary col-1.5" id="popover_user_activity" data-toggle="popover">Dodaj</button>
                    </div>
                </div>
                <div class="form-group">
                    <label for="desc">Opis djelatnosti:</label>
                    <input type="text" class="form-control" id="desc_user" name="desc" placeholder="Unesite opis djelatnosti">
                </div>
            </div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary"  name="submit">Sacuvaj</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
</div>
</form>
</div>
</div>
</div>
<div id="popover_user_activity_content" hidden="true">
    <form action="#" id="popover_add_form_user_activity" method="post">
        <div class="alert alert-warning" role="alert" id="pop_message_user_activity" hidden="true"></div>
        <div class="form-group">
            <label for="name_user_activity">Naziv djelatnosti:</label>
            <input type="text" class="border form-control" name="pop_name_user_activity" placeholder="Unesite naziv djelatnosti">
        </div>
        <div class="form-group">
            <label for="description_user_activity">Opis djelatnostii:</label>
            <input type="text" class="border form-control"  name="pop_description_user_activity" placeholder="Unesite opis djelatnosti">
        </div>
        <button type="button" class="btn btn-primary" id="pop_submit_user_activity" name="submit_user_activity">Sacuvaj</button>
        <button type="button" class="btn btn-secondary" id="pop_close_user_activity">Odustani</button>
    </form>
</div>

