<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/22/18
 * Time: 1:29 PM
 */
?>
<div class="modal fade" id="studentsActivity">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Aktivnost</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="#"  method="post" onsubmit="return checkBlankEditStudentActivity();" autocomplete="off">
                <div class="modal-body" id="edit_modal-body"></div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name="sub_student_activity">Sacuvaj</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
                </div>
            </form>
        </div>
    </div>
</div>