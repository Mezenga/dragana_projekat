<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/22/18
 * Time: 1:27 PM
 */
?>
<div class="modal fade" id="offer">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ponuda</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="#" id="edit_offer_form"  method="post" autocomplete="off">

                <div class="modal-body" id="offer-modal-body"></div>
                <div class="modal-footer">
                    <button type="button" id="edit_offer" class="btn btn-primary" name="sub_offer">Sacuvaj</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
                </div>
            </form>
        </div>
    </div>
</div>