<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/8/18
 * Time: 10:58 AM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once 'config/loader_models.php';
$usersModel=new usersModel();
if(isset($_POST['submit']))
{
    $usersModel->changeData($_POST,$_GET['id']);
    if($_SESSION['role']=='2')
    {
        if(($_GET['reg'])!="")
        {
            header('Location:index.php?view=userslist&reg=true');
        }
        else
        {
            header('Location:index.php?view=userslist');
        }
    }
    else
    {
        header('Location:index.php?view=userslist&reg=true');
    }
}
$result_id=$usersModel->users($_GET['id']);
$activityModel=new activityModel();
$type_activity=$activityModel->typeActivity();
if(!empty($result_id))
{
    $result=$usersModel->users($_GET['id']);
    $type_activity_id=$activityModel->typeActivity($result['idVrste']);
}
else
{
    header('Location:index.php?view=withoutview');
}
if($_SESSION['id']==$_GET['id'] || $_SESSION['role']=='2'):
?>
<div class="container">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
        <form action="#" method="POST" id="form_change_data_fuck" onsubmit="return CheckChangeData(<?=$_GET['id']; ?>);" autocomplete="off">
            <div id="message_data" class="alert alert-warning container" hidden="true"></div>
            <div class="form-group">
                <label for="name">Naziv:</label>
                <input type="text" class="form-control" id="name_edit_data" name="name" value="<?= $result['naziv'];?>">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="change_email" name="change_email" value="<?= $result['email'];?>">
            </div>
            <div class="form-group">
                <label for="address">Kontakt adresa:</label>
                <input type="text" class="form-control" id="address" name="address" value="<?= $result['kontakt_adresa'];?>">
            </div>
            <div class="form-group">
                <label for="web_site">Web sajt:</label>
                <input type="text" class="form-control" id="web_site" name="web_site" value="<?= $result['web_sajt'];?>">
            </div>
            <?php if($_SESSION['role']==1 || $_SESSION['id']!=$_GET['id']): ?>
            <div class="form-group">
                <label for="name_type">Vrsta djelatnosti:</label>
                <select class="custom-select" name="activity">
                    <option value="<?= $type_activity_id['idVrste']; ?>" title="<?= $type_activity_id['opis_djelatnosti']; ?>" selected><?= $type_activity_id['naziv_vrste_djelatnosti']; ?></option>
                    <?php foreach($type_activity as $activity):
                        if($activity['idVrste']!=$type_activity_id['idVrste']):?>
                        <option value="<?= $activity['idVrste']; ?>" title="<?= $activity['opis_djelatnosti']; ?>"><?= $activity['naziv_vrste_djelatnosti']; ?></option>
                    <?php endif; endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="desc">Opis djelatnosti:</label>
                <input type="text" class="form-control" id="desc" name="desc" value="<?= $result['opis_djelatnosti_korisnika'];?>">
            </div>
            <?php endif; ?>
        </div>
        <div class="col-3"></div>
    </div>
    <div class="row">
        <div class="col-3"></div>
        <div class="col-1">
            <button type="submit" name="submit" class="btn btn-primary">Potvrdi</button>
        </div>
        </form>
<div class="col-5">
    <?php if($_SESSION['id']==$_GET['id']): ?>
    <button type="button" class="btn btn-primary col-4 float-right" data-toggle="modal" data-target="#change_profile">
        Promjena lozinke

</div>

    <?php endif; ?>
        <div class="col-3"></div>
    </div>
</div>
<?php include FULL_FILE_PATH."Views/editprofile.php";
else:
    include FULL_FILE_PATH."Views/withoutview.php";
endif; ?>