<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/23/18
 * Time: 9:26 AM
 */
header('Content-Type: application/json');
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
if(HelperModel::isAjax()==true)
{
    $userModel=new usersModel();
    $users=$userModel->users();
    $exist=0;
    //$_GET['reg'] is set for ajax which will be used to check validation of input field of modal for registration
    if(isset($_GET['registration']))
    {
        //all input field have to be fill
        if($_POST['name']!="" && $_POST['password']!="" && $_POST['email']!="" && $_POST['address']!="" && $_POST['web_site']!="" && $_POST['desc']!="")
        {
            //input field for email must be in format of email
            if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
            {
                foreach($users as $user)
                {
                    //check if name or email already exist in database
                    if($user['naziv']==$_POST['name'] && $user['email']==$_POST['email'])
                    {
                        $exist++;
                        print json_encode(array('status' => false, 'message' => 'Naziv i email vec postoje u bazi!'));
                        die();
                    }
                    if($user['naziv']==$_POST['name'])
                    {
                        $exist++;
                        print json_encode(array('status' => false, 'message' => 'Naziv vec postoji u bazi!'));
                        die();
                    }
                    if($user['email']==$_POST['email'])
                    {
                        $exist++;
                        print json_encode(array('status' => false, 'message' => 'Email vec postoji u bazi!'));
                        die();
                    }
                }
                //return true only if all input fields are fill with correct value
                if($exist==0)
                {
                    print json_encode(array('status' => true));
                }
            }
            else
            {
                print json_encode(array('status' => false, 'message' => 'Niste unijeli dobar email!'));
            }
        }
        else
        {
            print json_encode(array('status' => false, 'message' => 'Popunite sva polja'));
        }
    }
}
?>
