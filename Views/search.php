<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/15/18
 * Time: 10:56 AM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'\dragana_projekat\config\loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
//this file use only for ajax  request when we searching users
if(HelperModel::isAjax()==true)
{
    $usersModel=new usersModel();
    //$_GET['reg'] is set for users whose registration is approved
    if($_GET['reg']!="")
    {
        $result=$usersModel->searchUser($_GET['search'],FALSE,TRUE);
    }
    else
    {
        $result=$usersModel->searchUser($_GET['search']);
    }
    $and_clause = '';
    if($_GET['reg']!="")
    {
        $default_url = 'index.php?view=userslist&reg=true';
        $pagination_url = 'index.php?view=userslist&reg=true&p=[p]&search='.$_GET['search'];
    }
    else
    {
        $default_url = 'index.php?view=userslist';
        $pagination_url = 'index.php?view=userslist&p=[p]&search='.$_GET['search'];
    }
    $search_term = '';
    $pageSize = 10;
    //set number of page on 1 or on parameters p if it is set
    if(isset($_GET['p']))
    {
        $pageNumber = $_GET['p'];
    }
    else
    {
        $pageNumber = 1;
    }
    $limitPage = ((int)$pageNumber - 1) * $pageSize;
    $limit_clause = " LIMIT ".$limitPage.",".$pageSize;
    $totalRecords = count($result);
    $pg = new bootPagination();
    $pg=HelperModel::setPagination($pg,$pageNumber,$pageSize,$totalRecords,$default_url,$pagination_url);
    if($_GET['reg']!="")
    {
        $result_us=$usersModel->searchUser($_GET['search'],$limit_clause,TRUE);
    }
    else
    {
        $result_us=$usersModel->searchUser($_GET['search'],$limit_clause,FALSE);
    }
    if(!empty($result_us)):
    ?>
    <div class="text-center" id="loading" hidden="true">
        <img src="<?= FULL_URL_PATH;?>Assets/icons/blue-loading-gif-transparent-9.gif">
    </div>
        <?php include_once FULL_FILE_PATH.'\Views\tables\table_userslist.php'; ?>
    <?php
        else:
    ?>
        <div class="alert alert-warning" role="alert">Nema rezultata!</div>
    <?php
        endif;
}
?>


