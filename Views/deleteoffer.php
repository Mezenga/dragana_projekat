<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/22/18
 * Time: 2:00 PM
 */
require_once FULL_FILE_PATH.'config/loader_models.php';
$offerModel=new offerModel();
if(isset($_GET['id']))
{
    $result=$offerModel->offerList($_GET['id']);
    if(!empty($result))
    {
        $offerModel->deleteOffer($_GET['id']);
        header('Location:index.php?view=offerlist');
    }
    else
    {
        header('Location:index.php?view=withoutview');
    }
}
