<?php
/**
 * Created by Dragana Cajic.
 * User: Dragana
 */
/**
 * Function users() return array of users.
 */
$usersModel=new usersModel();
$activityModel=new activityModel();
//$_GET['reg'] is set for users whose registration is approved
//for every result we have to check parameter $_GET['reg']
if(isset($_POST['submit']))
{
    $usersModel->addUser($_POST);
}
if(isset($_GET['reg']))
{
    $default_url = 'index.php?view=userslist&reg=true';
    $pagination_url = 'index.php?view=userslist&p=[p]&reg=true';
}
else
{
    $default_url = 'index.php?view=userslist';
    $pagination_url = 'index.php?view=userslist&p=[p]';
}
$and_clause = '';
$search_term = '';
$pageSize = 10;
$type_activity=$activityModel->typeActivity();
if(isset($_GET['reg']))
{
    $result=$usersModel->users(FALSE,FALSE,TRUE);
}
else
{
    $result=$usersModel->users();
}
//set number of page on 1 or get from url parameter p
if(isset($_GET['p']))
{
    $pageNumber = $_GET['p'];
}
else
{
    $pageNumber = 1;
}
$limitPage = ((int)$pageNumber - 1) * $pageSize;
$limit_clause = " LIMIT ".$limitPage.",".$pageSize;
//users who are result of search
if(isset($_GET['search']))
{
    if(isset($_GET['reg']))
    {
        $result=$usersModel->searchUser($_GET['search'],FALSE,TRUE);
        $result_us=$usersModel->searchUser($_GET['search'],$limit_clause,TRUE);
        $pagination_url = 'index.php?view=userslist&reg=true&p=[p]&search='.$_GET['search'];
        $default_url = 'index.php?view=userslist&reg=true&search='.$_GET['search'];
    }
    else
    {
        $result=$usersModel->searchUser($_GET['search']);
        $result_us=$usersModel->searchUser(FALSE,$limit_clause);
        $pagination_url = 'index.php?view=userslist&p=[p]&search='.$_GET['search'];
        $default_url = 'index.php?view=userslist&search='.$_GET['search'];
    }
}
//result of users when $_GET['search'] is not set
else
{
    if(isset($_GET['reg']))
    {
        $result_us=$usersModel->users(FALSE,$limit_clause,TRUE);
        $result=$usersModel->users(FALSE,FALSE,TRUE);
    }
    else
    {
        $result_us=$usersModel->users(FALSE,$limit_clause);
        $result=$usersModel->users();
    }
}
//when we want to display just one user and his id is set in url
if(isset($_GET['id']))
{
    $result=$usersModel->users($_GET['id']);
}
//for adding user

$totalRecords = count($result);
$pg = new bootPagination();
$pg=HelperModel::setPagination($pg,$pageNumber,$pageSize,$totalRecords,$default_url,$pagination_url);

?>
<div class="container">
    <br>
    <?php if(isset($_GET['reg'])): ?>
    <input id="reg_users" type="text" hidden="true" value="<?=$_GET['reg']; ?>">
    <?php else: ?>
    <input id="reg_users" type="text" hidden="true" value="">
    <?php endif; ?>
    <div class="row">
        <div class="col-6">
            <?php
            if($_SESSION['role']=='2' && !isset($_GET['reg'])): ?>
               <button type="button" class="btn btn-primary float-left" data-toggle="modal" data-target="#addUserModal">
                    Dodaj korisnika
                </button>
            <?php endif;
            if(!empty($result_us))
            {
            ?>
        </div>
        <div class="col-2">
            <img class="float-right" style="width: 20px;height: 30px;" src="<?= FULL_URL_PATH;?>Assets/icons/search.png">
        </div>
        <div class="col-3">
            <?php if(isset($_GET['reg'])): ?>
            <input id="reg" value="<?=$_GET['reg']; ?>" hidden="true">
        <?php else: ?>
        <input id="reg" hidden="true">
        <?php endif; ?>
            <?php if (isset($_GET['search'])):?>
                <input class="float-right form-control" type="search" placeholder="Pretraga..." id="search_users" value=<?= $_GET['search'];?>>
            <?php else:  ?>
                <input class="float-right form-control" type="search" placeholder="Pretraga..." id="search_users">
            <?php endif; ?>
        </div>
        <div class="col-1">
            <button type="button" class="btn btn-primary float-right" id="clear_search_users" disabled="true">Ponisti</button>
        </div>
    </div>
    <br>
    <div id="result">
        <?php include_once FULL_FILE_PATH.'Views/tables/table_userslist.php'; ?>
    </div>
</div>
    <?php

    }
    include FULL_FILE_PATH."Views/modals/adduser.php";

?>

