<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/22/18
 * Time: 2:39 PM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'\dragana_projekat\config\loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
//this file use only for ajax  request when we searching offers
if(HelperModel::isAjax()==true)
{
    $offerModel=new offerModel();
    $result=$offerModel->searchOffer($_GET['search']);
    $and_clause = '';
    $default_url = 'index.php?view=offerlist';
    $pagination_url = 'index.php?view=offerlist&p=[p]&search='.$_GET['search'];
    $search_term = '';
    $pageSize = 10;

    if(isset($_GET['p']))
    {
        $pageNumber = $_GET['p'];
    }
    else
    {
        $pageNumber = 1;
    }
    $limitPage = ((int)$pageNumber - 1) * $pageSize;
    $limit_clause = " LIMIT ".$limitPage.",".$pageSize;
    $totalRecords = count($result);
    $pg = new bootPagination();
    $pg=HelperModel::setPagination($pg,$pageNumber,$pageSize,$totalRecords,$default_url,$pagination_url);
    $offers=$offerModel->searchOffer($_GET['search'],$limit_clause);
    if(!empty($offers)):

        ?>
        <div class="text-center" id="loading_offers" hidden="true">
            <img src="<?= FULL_URL_PATH;?>Assets/icons/blue-loading-gif-transparent-9.gif">
        </div>
        <?php include_once FULL_FILE_PATH.'Views\tables\table_offerlist.php'; ?>
    <?php
    else:
        ?>
        <div class="alert alert-warning" role="alert">Nema rezultata!</div>
    <?php
    endif;
}
?>

