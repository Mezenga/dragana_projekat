<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/24/18
 * Time: 10:22 AM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
$offersModel=new offerModel();
$number_offers=count($offersModel->offersOfUser($_POST['id']));
if(isset($_POST['id']))
{
    $limit_clause=" LIMIT ".$_POST['start'].",".$_POST['limit'];
    $offers=$offersModel->offersOfUser($_POST['id'],$limit_clause);
}
if(HelperModel::isAjax()==true)
{
    if(isset($_POST['first']))
    {
        $number=1;
        foreach ($offers as $offer):
        ?>
            <div class="row">
                <div class="col border"><?= $number ?>.</div>
                <div class="col border"><a href=<?=FULL_URL_PATH;?>index.php?view=detailsoffer&id=<?=$offer['idPonude'];?>><?= $offer['naziv_tipa_aktivnosti'] ?></a></div>
                <div class="col border"><?= $offer['ciklus_studija'];?></div>
                <div class="col border"><?= $offer['rok_za_prijavu'];?></div>
                <div class="col border"><?= $offer['maksimalan_broj_kandidata'];?></div>
                <div class="col border"><?= $offer['trajanje'];?></div>

            </div>
    <?php
            $number++;
        endforeach;
    }
    else
    {
        $number=$_POST['start']+1;
        $data="";
        foreach ($offers as $offer)
        {
            $data.="
            <div class='row'>
                <div class='col border'>".$number++."</div>
                <div class='col border'><a href=".FULL_URL_PATH."index.php?view=detailsoffer&id=".$offer['idPonude'].">".$offer["naziv_tipa_aktivnosti"]."</a></div>
                <div class='col border'>".$offer["ciklus_studija"]."</div>
                <div class='col border'>".$offer['rok_za_prijavu']."</div>
                <div class='col border'>".$offer['maksimalan_broj_kandidata']."</div>
                <div class='col border'>".$offer['trajanje']."</div>
            </div>";

        }
        header('Content-Type: application/json');
        if(count($offers)==10)
        {
            print json_encode(array('content' => $data,'btn_hidden'=>'false'));
        }
        else
        {
            print json_encode(array('content' => $data,'btn_hidden'=>'true'));
        }
    }
}
?>
