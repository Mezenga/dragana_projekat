<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 8/27/18
 * Time: 9:39 AM
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/dragana_projekat/config/loader.php';
require_once FULL_FILE_PATH.'config/loader_models.php';
$activityModel=new activityModel();
$type_activity=$activityModel->typeActivity();
$userModel=new usersModel();
//when is submit and validate form for registration new user
if(isset($_POST['name']))
{
    $userModel->addUser($_POST);
    $users=$userModel->users();
    $admin=$userModel->adminInformation();
    $id_last_user=$usersModel->maxId();
    $link='http://localhost/dragana_projekat/index.php?view=details&id='.$id_last_user;
    $headers = "Content-type: text/html\r\n";
    $msg = "Postovani,<br><br>Stigla je registracija nove firme!<br>Naziv firme je: ".$_POST['name']."<br>Provjerite podatke i dozvolite registraciju na sledeci link:<a href=".$link.">Podaci o firmi</a>
    <br><br>Pozdrav,<br>Elektrotehnicki fakultet Istocno Sarajevo";
    if(mail($admin['email'],"Registracija nove firme!",$msg,$headers))
    {
        echo '</br><div style="width:500px;" class="alert alert-warning container" role="alert">Nakon odobrenja registracije bicete obavijesteni putem vase email adrese!
        </div>';
    }
    else
    {
        echo "error";
    }
}
