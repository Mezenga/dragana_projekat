<?php
/**
 * Created by Dragana Cajic.
 * User: Dragana
 */
    require_once FULL_FILE_PATH.'config/loader_models.php';
    $usersModel=new usersModel();
    $activityModel=new activityModel();
    $type_activity=$activityModel->typeActivity();
    //when is submit form for login
    if (isset($_POST['login']))
    {
        $check=$usersModel->login($_POST);
        if(isset($_GET['mail']))
        {
            header('Location:index.php?view=details&id=144');
        }
        //if information from form is name and password of some user from database
        if($check==true)
        {
            if(isset($_GET['mail']))
            {
                header('Location:index.php?view=details&id=144');
            }
            else
            {
                if($_SESSION['role']==2)
                {
                    header('Location:index.php?view=userslist&reg=true');
                }
                else
                {
                    header('Location:index.php?view=offerlist');
                }

            }
        }

        //if information from form is not name and password of any user from database
        else
        {

            echo '<div style="width:500px" class="alert alert-warning container">
            <strong>Upozorenje!</strong> Unijeli ste pogrešne ili nepotpune podatke!</div>';
        }

    }
?> 
<div class='container' style="width: 500px;">
    <div class="row">
        <div class="col-12">
	<form action="" method="post">
		<div class="form-group">
			<label for="email">Email:</label>
			<input type="email" class="form-control" id="email" placeholder="Unesite email" name="email">
    	</div>
		<div class="form-group">
			<label for="pwd">Lozinka:</label>
			<input type="password" class="form-control" id="pwd" placeholder="Unesite lozinku" name="pswd">
    	</div>
    </div>
    </div>
        <div class="row">
            <div class="col-3">
    	<button type="submit" class="btn btn-primary" name="login">Uloguj se</button>
            </div>
	</form>
<div class="col-5"></div>
<div class="col-4">
    <button type="button" class="btn btn-primary float-right" id="registration_button" data-toggle="modal" data-target="#mod_registration">
        Registracija
    </button>
</div>
        </div>
</div>
    <?php include FULL_FILE_PATH."Views/modals/registration.php";?>
    <?php include FULL_FILE_PATH."Views/registration.php"; ?>

