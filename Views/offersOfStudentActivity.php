<?php
/**
 * Created by PhpStorm.
 * User: Dragana
 * Date: 9/7/18
 * Time: 2:45 PM
 */
require_once FULL_FILE_PATH.'config/loader_models.php';
$default_url = 'index.php?view=offersOfStudentActivity&id='.$_GET['id'];
$pagination_url = 'index.php?view=offersOfStudentActivity&p=[p]&id='.$_GET['id'];
$and_clause = '';
$search_term = '';
$pageSize = 10;
if(isset($_GET['p']))
{
    $pageNumber = $_GET['p'];
}
else
{
    $pageNumber = 1;
}
$limitPage = ((int)$pageNumber - 1) * $pageSize;
$limit_clause = " LIMIT ".$limitPage.",".$pageSize;
$activityStudentModel=new activityOfStudentsModel();
$activity=$activityStudentModel->studentsActivity($_GET['id']);
if(empty($activity))
{
    header('Location:index.php?view=withoutview');
}
$offerModel=new offerModel();
$result_all=$offerModel->offersOfStudentActivity($_GET['id']);
$offers=$offerModel->offersOfStudentActivity($_GET['id'],$limit_clause);
$totalRecords = count($result_all);
$pg = new bootPagination();
$pg=HelperModel::setPagination($pg,$pageNumber,$pageSize,$totalRecords,$default_url,$pagination_url);
if(!empty($offers)):
?>
<div class="row">
    <div class="col-3">
    </div>
    <div class="col-6">
        <table class="table table-bordered table-hover">
            <thead class="text-center table-active">
            <tr class="table-active">
                <th>Redni broj</th>
                <th><?=$activity['naziv_tipa_aktivnosti']?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(isset($_GET['p']))
            {
                $number=($_GET['p']-1)*10+1;
            }
            else
            {
                $number=1;
            }
            foreach($offers as $offer):
                    ?>
                    <tr>
                        <td class='text-center table-active'><?= $number;?>.</td>
                        <td><a href=<?=FULL_URL_PATH;?>index.php?view=details&id=<?=$offer['idKorisnika'];?>><?=$offer['naziv'];?></a><a href=<?=FULL_URL_PATH;?>index.php?view=detailsoffer&id=<?=$offer['idPonude'];?> class="float-right">Informacije o ponudi</a></td>
                    </tr>
                    <?php
                    $number++;
            endforeach;
            ?>
            </tbody>
        </table>
    </div>
    <div class="col-3">
    </div>
</div>
<div class="row">
    <div class="col-3">
    </div>
    <div class="col-3 ">
        <?php
        echo $pg->process();
        ?>
    </div>
    <div class="col-3 float-right">
        <span class="float-right text-primary"> Ukupno:&nbsp;<?=$pg->totalrecords ?></span>
    </div>
    <div class="col-3">
    </div>
</div>
<?php else:?>
<div class="alert alert-warning" role="alert">Nema ponuda za aktivnost <?php print_r($activity['naziv_tipa_aktivnosti']); ?>!</div>
<?php endif; ?>